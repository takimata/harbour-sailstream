
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed
TARGET = harbour-sailstream

CONFIG += sailfishapp
# remove sailfishapp_i18n to disable building translations every time
CONFIG += sailfishapp_i18n

QT += multimedia core widgets dbus

CONFIG += glib
#CONFIG += c++17 # doesn't work with current Qt
QMAKE_CXXFLAGS += -std=c++17
# enable qDebug()
#CONFIG += warn_on

HEADERS += \
  src/resolutionquery.hpp \
  src/settings.hpp \
  src/streamer.hpp \
  src/streamermodel-dbusadaptor.hpp \
  src/streamermodel.hpp \
  src/streamstateqt.hpp

SOURCES += src/harbour-sailstream.cpp \
  src/resolutionquery.cpp \
  src/settings.cpp \
  src/streamer.cpp \
  src/streamermodel-dbusadaptor.cpp \
  src/streamermodel.cpp

DISTFILES += qml/harbour-sailstream.qml \
    qml/cover/CoverPage.qml \
    qml/pages/AboutPage.qml \
    qml/pages/ErrorDialog.qml \
    qml/pages/FirstRead.qml \
    qml/pages/MainPage.qml \
    images/background.png \
    rpm/harbour-sailstream.changes \
    rpm/harbour-sailstream.changes.run.in \
    rpm/harbour-sailstream.spec \
    rpm/harbour-sailstream.yaml \
    translations/*.ts \
    receiver/sailstream-recv.py \
    harbour-sailstream.desktop

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

TRANSLATIONS += translations/harbour-sailstream-de.ts

coverimage.files = images
coverimage.path = /usr/share/$${TARGET}

receiverparts.files = receiver/sailstream-recv.py
receiverparts.path = /usr/share/$${TARGET}

INSTALLS += receiverparts coverimage

CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT

QMAKE_CXXFLAGS += -Wall -Wextra
#QMAKE_CXXFLAGS += -O0 -g

#CONFIG += link_pkgconfig # BUG: this breaks linking with libsailfishapp but fortunately pkgconfig is already enabled
PKGCONFIG += gstreamer-1.0

# just for QtCreator and glibconfig.h
INCLUDEPATH += "/usr/lib/x86_64-linux-gnu/glib-2.0/include/"

#SUBDIRS += \
#  sender-cli/sailstream-cli.pro
#OTHER_FILES += \
#  sender-cli/sailstream-cli.pro

OTHER_FILES += README.md
