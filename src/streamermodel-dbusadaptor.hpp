#ifndef STREAMERMODELDBUSADAPTOR_HPP
#define STREAMERMODELDBUSADAPTOR_HPP

#include <QObject>
#include <QDBusAbstractAdaptor>
#include <QDBusMetaType>
#include <QVector>

#include <vector>
#include "streamermodel.hpp"

#define DBUSINTERFACENAME "org.harbour.sailstreamv0.Stream"
Q_DECLARE_METATYPE(QVector<int>)  // make QVector<int> available as DBus type (step 1/2)

/**
 * @brief DBus adapter for StreamerModel
 * WARNING: all slots are exposed as D-Bus interface!
 */
class StreamerModelDBusAdaptor : public QDBusAbstractAdaptor {
  Q_OBJECT
  Q_CLASSINFO("D-Bus Interface", DBUSINTERFACENAME)

  private:
  StreamerModel *streamer;  // is a QObject

  public:
  StreamerModelDBusAdaptor(StreamerModel *streamer);

  public slots:
  void refocusCamera(int mode = -1);
  /*Q_SCRIPTABLE*/ QVector<int> getSupportedFocusModes();
};

#endif  // STREAMERMODELDBUSADAPTOR_HPP
