#ifndef STREAMSTATEQT_HPP
#define STREAMSTATEQT_HPP
#include <stdexcept>
#include <QObject>
#include "streamer.hpp"

class StreamStateQt : public QObject {
  Q_OBJECT

  public:
  // Ugh...
  // Without adding Qt in some way, it's impossible to to use a scoped enum from C++ in QML
  // Because of Qt's MOC, it's even nearly impossible to #ifndef out Qt if it isn't needed
  enum class State {
    Standby = Streamer::Standby,
    Connecting = Streamer::Connecting,
    Connected = Streamer::Connected,
    Streaming = Streamer::Streaming,
    Reconfiguring = Streamer::Reconfiguring,
    Stopping = Streamer::Stopping,
    Stopped = Streamer::Stopped,
    Error = Streamer::Error
  };
  Q_ENUM(State);

  StreamStateQt() : QObject() {
  };

  static State map(Streamer::StreamState s) {
    switch (s) {
     case Streamer::StreamState::Standby:
       return State::Standby;
     case   Streamer::StreamState::Connecting:
       return State::Connecting;
     case Streamer::StreamState::Connected:
       return State::Connected;
     case Streamer::StreamState::Streaming:
       return State::Streaming;
     case Streamer::StreamState::Reconfiguring:
       return State::Reconfiguring;
     case Streamer::StreamState::Stopping:
       return State::Stopping;
     case Streamer::StreamState::Stopped:
       return State::Stopped;
     case Streamer::StreamState::Error:
       return State::Error;
     default:
       throw std::logic_error("BUG");
    }
  }

  signals:

  public slots:
};

#endif  // STREAMSTATEQT_HPP
