#include <iostream>
#include "streamermodel.hpp"

StreamerModel::StreamerModel(bool useMKI, const QString& dstHost, QObject *parent) : QObject(parent),
  streamer{useMKI,
           Streamer::MediaSelection::audiovideo,
           dstHost.toStdString(),
           nullptr,
           std::bind(&StreamerModel::consumeStreamStateChanged, this),
           std::bind(&StreamerModel::consumeError, this, std::placeholders::_1)} {
}

void StreamerModel::consumeStreamFinished() {
}
void StreamerModel::consumeStreamStateChanged() {
  emit streamStateChanged();
}
void StreamerModel::consumeError(const std::string& msg) {
  emit errorMsg(QString(msg.c_str()));
}

StreamStateQt::State StreamerModel::getState() const {
  return StreamStateQt::map(streamer.getState());
}

void StreamerModel::start() {
  streamer.start();
}

void StreamerModel::stop() {
  streamer.stop();
}

void StreamerModel::refocusCamera(int mode) {
  streamer.refocusCamera(mode);
}

void StreamerModel::changeDestination(const QString& dstHost) {
  streamer.changeDestination(dstHost.toStdString());
}

void StreamerModel::changeResolution(int width, int height) {
  streamer.changeResolution(width, height);
}

std::shared_ptr<std::vector<int> > StreamerModel::getSupportedFocusModes() {
  return streamer.getSupportedFocusModes();
}
