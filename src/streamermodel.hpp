#ifndef STREAMERMODEL_HPP
#define STREAMERMODEL_HPP

#include <QObject>
#include <QString>
#include <memory>

#include "streamer.hpp"
#include "streamstateqt.hpp"

class StreamerModel : public QObject {
  Q_OBJECT

  Q_PROPERTY(StreamStateQt::State state READ getState NOTIFY streamStateChanged)

  // having this here works because Qt on Linux runs a GLib event loop behind the scenes
  Streamer streamer;

  void consumeStreamFinished();
  void consumeStreamStateChanged();
  void consumeError(const std::string& errorMsg);

  public:
  explicit StreamerModel(bool useMKI, const QString& dstHost, QObject *parent = nullptr);
  StreamStateQt::State getState() const;

  signals:
  void streamStateChanged();
  void errorMsg(QString errorDesc);

  public slots:
  void start();
  void stop();

  void refocusCamera(int mode = -1);
  void changeDestination(const QString& dstHost);
  void changeResolution(int width, int height);

  std::shared_ptr<std::vector<int> > getSupportedFocusModes();
};

#endif  // STREAMERMODEL_HPP
