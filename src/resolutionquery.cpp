
#include <QObject>
#include <QMediaRecorder>
#include <QCamera>
#include <QCameraInfo>
#include <QCameraImageCapture>
#include <QEventLoop>

#include <numeric>
#include <iostream>
#include <algorithm>

#include "resolutionquery.hpp"

const QString ResolutionQuery::defaultMediaRecorderContainerFormat("video/mp4");

ResolutionQuery::ResolutionQuery() {
}

bool ResolutionQuery::qsizeGreaterThan(const QSize &lhs, const QSize &rhs) {
  if (lhs.width() == rhs.width()) {
    return lhs.height() > rhs.height();
  } else {
    return lhs.width() > rhs.width();
  }
}

void ResolutionQuery::init() {
  if (cached.get() != nullptr && cached->size() > 0) {
    return;
  }
  cached = supportedVideoResolutions("primary");

  beginResetModel();
  supportedResolutions.clear();

  for (const Resolution& res: *cached) {
    supportedResolutions.append(QSize(res.width, res.height));
  }

  std::sort(supportedResolutions.begin(), supportedResolutions.end(), qsizeGreaterThan);

  if (supportedResolutions.size() > 0) {
    qDebug() << "Supported video resolutions (#" << supportedResolutions.count() << "): " << supportedResolutions;
  } else {
    qWarning() << "Couldn't query video resolutions!";
  }

  endResetModel();
  emit rowCountChanged();
}

const QString ResolutionQuery::getDefaultMediaRecorderContainerFormat(QObject *recorder) {
  QMediaRecorder *r = nullptr;
  QList<QMediaRecorder *> recorders = recorder->findChildren<QMediaRecorder *>();

  if (recorders.count() > 0) {
    r = recorders[0];
    if (r != nullptr && !r->supportedContainers().isEmpty()) {
      return r->supportedContainers().constFirst();
    }
  }
  return defaultMediaRecorderContainerFormat;
}

QHash<int, QByteArray> ResolutionQuery::roleNames() const {
  QHash<int, QByteArray> roles = QAbstractItemModel::roleNames();  // associates Qt::DisplayRole with "display"
  roles[ResolutionValue] = "value";
  return roles;
}

int ResolutionQuery::rowCount(const QModelIndex &parent) const {
  Q_UNUSED(parent);
  return supportedResolutions.count();
}

static QSize sizeToRatio(const QSize &size) {
  int gcd = std::gcd(size.width(), size.height());
  return (gcd == 0) ? size : (size / gcd);
}

QVariant ResolutionQuery::data(const QModelIndex &index, int role) const {
  QVariant empty;

  if (!index.isValid() || index.row() > rowCount(index) || index.row() < 0) {
    return empty;
  }

  const QSize &res = supportedResolutions.at(index.row());

  switch (role) {
   case  Qt::DisplayRole: {
     const QSize &ratio = sizeToRatio(supportedResolutions.at(index.row()));
     return QVariant(QString("%1x%2 (%3:%4)").arg(res.width()).arg(res.height()).arg(ratio.width()).arg(ratio.height()));
   }
   case ResolutionValue:
     return QVariant(res);
   case Qt::InitialSortOrderRole:
     return QVariant(res.width() * res.height());
   default:
     break;
  }

  return empty;
}

uint ResolutionQuery::indexOf(const QSize &resolution) {
  uint i = 0;
  init();
  const QList<QSize> & constResolutions = supportedResolutions;
  for (const QSize& res: constResolutions) {
    if (res == resolution) {
      return i;
    }
    i++;
  }
  return 0;
}

/** DEPRECATED
 * @brief ResolutionQuery::setMediaRecorder
 * @param recobj
 */
// void ResolutionQuery::setMediaRecorder(QObject *recobj) {
//  QMediaRecorder *recorder = nullptr;
//  QList<QMediaRecorder *> recs = recobj->findChildren<QMediaRecorder *>();

//  if (recs.count() > 0) {
//    recorder = recs[0];
//  }

//  if (recorder != nullptr) {
//    QList<QSize> result = recorder->supportedResolutions();
//    qDebug() << "supportedResolutions: #" << result.count() << ": " << result;

//    beginResetModel();
//    supportedResolutions.clear();

//    for (const QSize& size: result) {
//      if (!supportedResolutions.contains(size)) {  // we get some duplicates, which we remove here
//        // TODO: maybe this is a bug in Qt/gstreamer confusing resolutions AxB and BxA ?
//        supportedResolutions.append(size);
//      }
//    }

//    endResetModel();
//    emit rowCountChanged();

//    if (supportedResolutions.size() > 0) {
//      qDebug() << "Supported video resolutions (#" << supportedResolutions.count() << "): " << supportedResolutions;
//    } else {
//      qWarning() << "Couldn't query video resolutions!";
//    }
//  }
// }

QSize ResolutionQuery::defaultResolution() {
  if (supportedResolutions.count() > 0) {
    return supportedResolutions.at(supportedResolutions.count() - 1);
  }

  return QSize(0, 0);
}

bool ResolutionQuery::isValidResolution(const QSize &resolution) const {
  return supportedResolutions.contains(resolution);
}

const Resolutions ResolutionQuery::supportedVideoResolutions() {
  init();
  return cached;
}

const Resolutions ResolutionQuery::supportedVideoResolutions(const std::string& deviceName) const {
  {  // this is necessary for QCameraInfo::availableCameras() to return a non-empty list
    QCamera foo(QCamera::Position::BackFace);
    // foo.setCaptureMode(QCamera::CaptureVideo);
  }

  auto resolutions = std::make_shared<std::set<Resolution, ResolutionCompare> >();
  const QList<QCameraInfo> cameras = QCameraInfo::availableCameras();
  QEventLoop loop;

  const auto handler = [this, &loop, &resolutions](QCamera::Status status) {
                         if (status == QCamera::Status::StartingStatus) {
                           QCamera *cam = qobject_cast<QCamera *>(sender());
                           Q_ASSERT(cam != nullptr);

                           QMediaRecorder recorder(cam);
                           // TODO: support arbitrary res. within supported range (does any Sailfish device offer this?)
                           bool continuous = false;

                           recorder.setMuted(true);
                           recorder.setContainerFormat(getDefaultMediaRecorderContainerFormat(&recorder));
                           QVideoEncoderSettings settings;
                           settings.setFrameRate(30);
                           settings.setEncodingMode(QMultimedia::EncodingMode::AverageBitRateEncoding);
                           settings.setCodec(QStringLiteral("video/x-h264"));
                           recorder.setVideoSettings(settings);

                           // supportedResolutions() likes to crash if camera set up improperly
                           QList<QSize> result = recorder.supportedResolutions(QVideoEncoderSettings(), &continuous);
                           for (const QSize& size: result) {
                             resolutions->emplace(size.width(), size.height());
                           }

                           qDebug() << "Supported video resolutions: " << result;
                           qDebug() << "Resolutions continuous: " << continuous;

                           cam->stop();
                           cam->unload();
                         } else if (status == QCamera::Status::UnloadedStatus) {
                           loop.quit();
                         }
                       };

  for (const QCameraInfo &cameraInfo : cameras) {
    if (cameraInfo.deviceName().toStdString() == deviceName) {
      QCamera cam(cameraInfo);
      cam.setCaptureMode(QCamera::CaptureVideo);
      // querying Qt about the hardware's resolution capabilities is delicate:
      // having the cam loaded is not enough and doing it the wrong way crashes horribly
      cam.start();

      connect(&cam, &QCamera::statusChanged, this, handler);
      connect(&cam, static_cast<void (QCamera::*)(QCamera::Error)>(&QCamera::error),
        [&loop](QCamera::Error value) {
        qWarning() << "Error " << value << " querying resolution";
        loop.quit();
      });
      loop.exec();
      break;
    }
  }

  return resolutions;
}
