#include "streamer.hpp"

#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <cassert>
#include <sstream>
#include <functional>
#include <algorithm>
#include <memory>

#include <glib.h>
#include <gst/gst.h>

#include <unistd.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <poll.h>
#include <stdio.h>
#include <string.h>
#include <utility>

// custom signal names
#define KEYSRV_CONNECT_DONE "harbour-sailstream-keysrv-connect-done"
#define DROIDCAM_STOPPED "harbour-sailstream-camera-stopped"

// explanations at https://github.com/GStreamer/gst-plugins-bad/blob/master/gst-libs/gst/interfaces/photography.h#L379
const std::map<int, std::string> Streamer::focusModeNames = {{GST_PHOTOGRAPHY_FOCUS_MODE_AUTO, "Auto"},
  {GST_PHOTOGRAPHY_FOCUS_MODE_MACRO, "Macro"},
  {GST_PHOTOGRAPHY_FOCUS_MODE_PORTRAIT, "Portrait"},
  {GST_PHOTOGRAPHY_FOCUS_MODE_INFINITY, "Infinity"},
  {GST_PHOTOGRAPHY_FOCUS_MODE_HYPERFOCAL, "Hyperfocal"},
  {GST_PHOTOGRAPHY_FOCUS_MODE_EXTENDED, "Extended"},
  {GST_PHOTOGRAPHY_FOCUS_MODE_CONTINUOUS_NORMAL, "Continuous-normal"},
  {GST_PHOTOGRAPHY_FOCUS_MODE_CONTINUOUS_EXTENDED, "Continuous-extended"},
  {GST_PHOTOGRAPHY_FOCUS_MODE_MANUAL, "Manual"}};

Streamer::Streamer(bool useMKI, MediaSelection m, const std::string& dstHost, FinishedCb onFinished,
                   StreamerStateCallback onStatusChangedCallback, ErrorCb onError)
  : supportedFocusModes(std::make_shared<std::vector<int> >()), useMKI(useMKI), ms{m}, dstHost{dstHost}, onStatusChanged(std::move(onStatusChangedCallback)), onError(std::move(onError)), onFinished(std::move(onFinished)) {

  pipeline = gst_pipeline_new(nullptr);
  // create custom signals (instead of creating an own object)
  g_signal_new(KEYSRV_CONNECT_DONE, G_TYPE_FROM_INSTANCE(pipeline), G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL, G_TYPE_NONE, 1, G_TYPE_POINTER);
  g_signal_connect(pipeline, KEYSRV_CONNECT_DONE, G_CALLBACK(&startPipelines), this);

  GstElementFactory *droidcamfac = gst_element_factory_find("droidcamsrc");
  gst_plugin_feature_load(GST_PLUGIN_FEATURE_CAST(droidcamfac));
  GType t = gst_element_factory_get_element_type(droidcamfac);
  if (t == 0) {
    g_printerr("droidcamsrc factory not loaded\n");
    exit(EXIT_FAILURE);
  }
  g_signal_new(DROIDCAM_STOPPED, t, G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL, G_TYPE_NONE, 1, G_TYPE_POINTER);
  gst_object_unref(droidcamfac);
}

Streamer::~Streamer() {
  if (sock != -1) {
    close(sock);
  }
  if (pipeline) {
    gst_element_set_state(pipeline, GST_STATE_NULL);
    gst_object_unref(pipeline);  // Bin destroys all its child elements
  }
}

void Streamer::postErrorMsg(const std::string& msg) {
  if (onError != nullptr) {
    onError(msg);
  }
}

void Streamer::setState(Streamer::StreamState newState) {
  if (state == newState) {
    return;
  }
  state = newState;
  if (onStatusChanged != nullptr) {
    onStatusChanged();
  }
}

Streamer::StreamState Streamer::getState() const {
  return state;
}

// https://github.com/sailfishos/gst-droidcamsrc/blob/master/gst/droidcamsrc/gstvidsrcpad.c#L39
/** There currently is a way to query droidcamsrc for allowed resolutions, but only for still images.
 * For now, this application falls back to Qt...
 * Probably a HAL/android interface also exists to get this info, but I haven't found it yet
 * @brief Streamer::printResolutionInfo
 * @param user_data
 * @return
 */
// gboolean Streamer::printResolutionInfo(gpointer user_data) {
//  Streamer *thisptr = static_cast<Streamer *>(user_data);
//  // https://gstreamer.freedesktop.org/documentation/gstreamer/gstcaps.html#gst_caps_foreach
//  if (thisptr->vidsrc != nullptr) {
//    g_print("printInfo\n");
//    GObject *obj = nullptr;
//    // resolutions only valid for still images, unfortunately not valid for video...
//    g_object_get(thisptr->vidsrc, "image-capture-supported-caps", &obj, NULL);
//    GST_ERROR("image-capture-supported-caps are \n%" GST_PTR_FORMAT, (GstCaps *)obj);
//    gst_caps_unref((GstCaps *)obj);

//    // and image-preview-supported-caps just gives null...
//    return TRUE;
//  }
//  return FALSE;  // stop calling callback
// }

gboolean Streamer::start_camera_cb(gpointer user_data) {
  Streamer *thisptr = static_cast<Streamer *>(user_data);
  g_signal_emit_by_name(thisptr->camsrc, "start-capture", NULL);
  thisptr->setState(StreamState::Streaming);
  return FALSE;  // stop calling callback
}

gboolean Streamer::stop_camera_cb(gpointer user_data) {
  Streamer *thisptr = static_cast<Streamer *>(user_data);
  g_signal_emit_by_name(thisptr->camsrc, "stop-capture", NULL);
  return FALSE;  // stop calling callback
}

gboolean Streamer::bus_msg_handler(__attribute__((unused)) GstBus *bus, GstMessage *msg, gpointer data) {
  Streamer *thisptr = static_cast<Streamer *>(data);

  switch (GST_MESSAGE_TYPE(msg)) {
   case GST_MESSAGE_EOS:
     g_printerr("End of stream\n");
     if (thisptr->onFinished != nullptr) {
       thisptr->onFinished();
     }
     // the camera might be running/in an error loop, so stop it
     gst_element_set_state(thisptr->camsrc, GST_STATE_NULL);
     break;

   case GST_MESSAGE_ERROR: {
     gchar *debug;
     GError *error;

     gst_message_parse_error(msg, &error, &debug);
     g_printerr("Error: %s (%s)\n", error->message, debug);
     std::string err(error->message);
     err.append("\n");
     err.append(debug);
     thisptr->postErrorMsg(err);
     g_error_free(error);
     if (debug) {
       g_free(debug);
     }

     thisptr->stop();
     thisptr->setState(StreamState::Error);
     if (thisptr->onFinished != nullptr) {
       thisptr->onFinished();
     }
     break;
   }

   case GST_MESSAGE_WARNING: {
     gchar *debug;
     GError *error;

     gst_message_parse_warning(msg, &error, &debug);
     g_printerr("Warning: %s (%s)\n", error->message, debug);
     g_error_free(error);

     if (debug) {
       g_free(debug);
     }

     // g_print("Latency: %llu (ns ?) \n", gst_pipeline_get_latency(GST_PIPELINE(pipeline->pipeline)));
     break;
   }

   case GST_MESSAGE_PROPERTY_NOTIFY:  // check notifications: camera running?
     if (GST_MESSAGE_SRC(msg) == (GstObject *)thisptr->camsrc) {
       const gchar *propertyName = nullptr;
       const GValue *propertyValue = nullptr;
       gst_message_parse_property_notify(msg, NULL, &propertyName, &propertyValue);
       if (g_strcmp0(propertyName, "ready-for-capture") == 0 && propertyValue != nullptr) {
         // this property is true iff the camera is not capturing
         if (G_VALUE_HOLDS_BOOLEAN(propertyValue) && g_value_get_boolean(propertyValue)) {
           g_signal_emit_by_name(thisptr->camsrc, DROIDCAM_STOPPED, thisptr);
         }
       }
     }
     break;

   case GST_MESSAGE_STATE_CHANGED: {
     if (GST_MESSAGE_SRC(msg) == (GstObject *)thisptr->camsrc) {
       g_print("Got message of type %s from %s\n", GST_MESSAGE_TYPE_NAME(msg), GST_MESSAGE_SRC_NAME(msg));
       GstState oldState;
       gst_message_parse_state_changed(msg, &oldState, nullptr, nullptr);
       const GstStructure *s = gst_message_get_structure(msg);
       GST_INFO("structure is %" GST_PTR_FORMAT, (void *)s);
     }
   }
   break;

   default:
     if (GST_MESSAGE_SRC(msg) == (GstObject *)thisptr->camsrc) {
       g_print("Got message of type %s from %s\n", GST_MESSAGE_TYPE_NAME(msg), GST_MESSAGE_SRC_NAME(msg));
       const GstStructure *s = gst_message_get_structure(msg);
       GST_INFO("structure is %" GST_PTR_FORMAT, (void *)s);
     }
     break;
  }

  return TRUE;
}

void Streamer::startPipelines( GstElement *  /*pipeline*/, gpointer udata_this) {
  Streamer *thisptr = static_cast<Streamer *>(udata_this);
  // we are already connected to the key server and probably set up
  if (thisptr->videoUdpSink == nullptr || thisptr->audioUdpSink == nullptr) {
    throw std::logic_error("Starting but not initialized");
  }

  // request first keys for video and continue with audio part when done
  need_new_key_callback(thisptr, true, [thisptr](GstElement *srtpenc, const struct KeyMsg *keyMsg) {
    thisptr->setNewKey(srtpenc, keyMsg);  // set first keys for video

    // request first keys for audio
    need_new_key_callback(thisptr, false, [thisptr](GstElement *srtpenc, const struct KeyMsg *keyMsg) {
      thisptr->setNewKey(srtpenc, keyMsg);

      gst_pipeline_set_latency(GST_PIPELINE(thisptr->pipeline), GST_CLOCK_TIME_NONE);

      // g_timeout_add_seconds(3, printResolutionInfo, thisptr);
      if (gst_element_set_state(thisptr->pipeline, GST_STATE_PLAYING) == GST_STATE_CHANGE_FAILURE) {
        g_printerr("Failed to set pipeline state to PLAYING");
        thisptr->setState(StreamState::Error);
      }
      start_camera_cb(thisptr);
    });
  });
}

/** Defer initialization because it takes some time
 */
void Streamer::ensureSetupPipelines() {
  if (videoUdpSink != nullptr || audioUdpSink != nullptr) {
    return;  // already initialized
  }

  if (ms == MediaSelection::audiovideo || ms == MediaSelection::video) {
    if (setupVideoPipeline() != 0) {
      gst_clear_object(&pipeline);
      close(sock);
      throw std::runtime_error("Video pipeline creation error: Missing dependencies?");
    }
  }

  if (ms == MediaSelection::audiovideo || ms == MediaSelection::audio) {
    if (setupAudioPipeline() != 0) {
      gst_clear_object(&pipeline);
      close(sock);
      throw std::runtime_error("Audio pipeline creation error: Missing dependencies?");
    }
  }

  GstBus *bus = gst_pipeline_get_bus(GST_PIPELINE(pipeline));
  gst_bus_add_watch(bus, bus_msg_handler, this);
  gst_object_unref(bus);
}

void Streamer::connectKeyClient() {
  if (sock != -1) {
    return;  // already connected
  }
  g_print("Connecting to key server...\n");
  setState(StreamState::Connecting);
  struct addrinfo hints;
  struct addrinfo *result = nullptr;

  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_UNSPEC;  // Allow IPv4 or IPv6
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = 0;  // AI_PASSIVE not set, we'll get the loopback addr (localhost)
  hints.ai_protocol = 0;
  hints.ai_canonname = nullptr;
  hints.ai_addr = nullptr;
  hints.ai_next = nullptr;
  int s = getaddrinfo(nullptr, STR(TCPKEYSERVERPORT), &hints, &result);
  if (s != 0) {
    fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
    exit(EXIT_FAILURE);
  }

  char addrstring[INET6_ADDRSTRLEN] = "";

  if (result == nullptr) {
    perror("Failed to getaddrinfo()");
    exit(EXIT_FAILURE);
  }

  sock = socket(result->ai_family, result->ai_socktype | SOCK_NONBLOCK, result->ai_protocol);
  if (sock == -1) {
    perror("Could not create socket");
    exit(EXIT_FAILURE);
  }

  if (result->ai_family == AF_INET) {
    inet_ntop(result->ai_family, &((struct sockaddr_in *)result->ai_addr)->sin_addr, addrstring, sizeof(addrstring));
  } else if (result->ai_family == AF_INET6) {
    inet_ntop(result->ai_family, &((struct sockaddr_in6 *)result->ai_addr)->sin6_addr, addrstring, sizeof(addrstring));
  }

  fprintf(stdout, "Trying to connect to key server at %s:" STR(TCPKEYSERVERPORT) "\n", addrstring);

  if (connect(sock, result->ai_addr, result->ai_addrlen) == 0) {
    onConnectFinished(nullptr, G_IO_OUT, this);
  } else {
    int connectErrno = errno;
    if (connectErrno == EINPROGRESS) {
      // wait for completion of connect by polling for writable
      GIOChannel *channel = g_io_channel_unix_new(sock);
      g_io_add_watch(channel, G_IO_OUT, &onConnectFinished, this);
      g_io_channel_unref(channel);
    } else {
      close(sock);
      sock = -1;
      g_printerr("error connecting");
      fprintf(stderr, "Could not connect to key server: %s\n", strerror(connectErrno));
      exit(EXIT_FAILURE);
    }
  }
  freeaddrinfo(result);
}

gboolean Streamer::onConnectFinished(GIOChannel *  /*source*/, GIOCondition condition, gpointer udata_this) {
  Streamer *thisptr = static_cast<Streamer *>(udata_this);
  assert(condition == G_IO_OUT);

  int error = -1;
  size_t len = sizeof(error);

  if (getsockopt(thisptr->sock, SOL_SOCKET, SO_ERROR, &error, &len) == -1) {
    throw std::logic_error("getsockopt() failed, exiting.");
  }

  if (error == 0) {
    thisptr->setState(StreamState::Connected);
    GIOChannel *sockChannel = g_io_channel_unix_new(thisptr->sock);
    g_io_add_watch(sockChannel, G_IO_IN, (GIOFunc) & onKeyDataReceived, thisptr);
    g_io_channel_unref(sockChannel);

    g_signal_emit_by_name(thisptr->pipeline, KEYSRV_CONNECT_DONE, thisptr);
  } else {
    thisptr->sock = -1;
    fprintf(stderr, "Could not connect to key server: %s\n", strerror(error));
    std::ostringstream stringStream;
    stringStream << "Could not connect to key server: " << strerror(error);
    thisptr->postErrorMsg(stringStream.str());
    thisptr->setState(StreamState::Standby);
  }
  return FALSE;
}

void Streamer::start() {
  if (state != StreamState::Standby) {
    setState(StreamState::Error);
    return;
  }
  ensureSetupPipelines();

  g_print("Sending SRTP to %s:" STR(UDPVIDEOPORT) " and " STR(UDPAUDIOPORT) "\n", dstHost.c_str());
  connectKeyClient();
}

void Streamer::stop() {
  if (state != StreamState::Connected && state != StreamState::Streaming && state != StreamState::Reconfiguring) {
    return;
  }

  g_print("Stopping pipeline...\n");
  setState(StreamState::Stopping);
  stop_camera_cb(this);  // camera does not stop on end of stream
  gst_element_send_event(pipeline, gst_event_new_eos());
  if (sock != -1) {
    close(sock);
    sock = -1;
  }
  // TODO going to Standby works in principle, just the camera crashes...
  setState(StreamState::Stopped);
}

static void printHex(const uint8_t *array, size_t len) {
  for (size_t i = 0; i < len; i++) {
    fprintf(stdout, "%02x", array[i]);
  }
}

void Streamer::setNewKey(GstElement *srtpenc, const struct KeyMsg *keyMsg) {
  GstBuffer *buffer = gst_buffer_new_allocate(NULL, sizeof(keyMsg->key), NULL);
  gst_buffer_fill(buffer, 0, keyMsg->key, sizeof(keyMsg->key));
  g_object_set(srtpenc, "key", buffer, NULL);
  gst_buffer_unref(buffer);

  // Compatibility mode: If the receiver runs Gstreamer version < 1.16, the sender must use mki=null so that the
  // receiver can properly unprotect the data (support for MKIs included in GStreamer 1.16 or higher)
  if (useMKI) {
    buffer = gst_buffer_new_allocate(NULL, sizeof(keyMsg->mki), NULL);
    gst_buffer_fill(buffer, 0, keyMsg->mki, sizeof(keyMsg->mki));
    g_object_set(srtpenc, "mki", buffer, NULL);
    gst_buffer_unref(buffer);
  } else {
    g_object_set(srtpenc, "mki", NULL, NULL);
  }
}

gboolean Streamer::onKeyDataReceived(__attribute__((unused)) GIOChannel *source, __attribute__((unused)) GIOCondition condition, gpointer udata_this) {
  Streamer *thisptr = static_cast<Streamer *>(udata_this);
  struct KeyMsg keydata;

  int result = recv(thisptr->sock, &keydata, sizeof(keydata), MSG_DONTWAIT | MSG_PEEK);  // avoid blocking while reading
  if (result != sizeof(keydata)) {
    return TRUE;
  }

  result = recv(thisptr->sock, &keydata, sizeof(keydata), MSG_WAITALL);
  if (0 == result) {
    printf("Looks like the server has closed the connection\n");
    exit(1);
  } else if (result != sizeof(keydata)) {
    fprintf(stderr, "recv() failed\n");
    exit(1);
  }

  // char tmp[] = "012345678901234567890123456789012345678901234567890123456789012345";
  // memcpy(&keydata.key, &tmp, keylen);
  // keydata.mki[0] = 0;

  printf("Received new key (in hex):\n");
  printHex(keydata.key, sizeof(keydata.key));
  printf(" with mki ");
  printHex(keydata.mki, sizeof(keydata.mki));
  printf("\n");

  switch (keydata.type) {
   case 'A':
     if (!thisptr->audioKeyConsumer) {
       fprintf(stderr, "Protocol violation - unsolicited akey data\n");
       exit(1);
     }

     thisptr->audioKeyConsumer(thisptr->audioSRTPenc, &keydata);
     thisptr->audioKeyConsumer = nullptr;
     break;
   case 'V':
     if (!thisptr->videoKeyConsumer) {
       fprintf(stderr, "Protocol violation - unsolicited vkey data\n");
       exit(1);
     }
     thisptr->videoKeyConsumer(thisptr->videoSRTPenc, &keydata);
     thisptr->videoKeyConsumer = nullptr;
     break;
   default:
     fprintf(stderr, "Protocol violation\n");
     exit(1);
  }

  return TRUE;
}

/** This GLib signal handler is executed in the GLib main loop and provides keys to the srtp encoders
 * @brief need_new_key_callback
 * @param srtpenc
 * @param udata
 * @param isVideo
 */
void Streamer::need_new_key_callback(gpointer udata_this, bool isVideo, KeyMsgConsumer cb) {
  Streamer *thisptr = static_cast<Streamer *>(udata_this);

  static const char requestVideoKey[] = "GetNextKeyV";
  static const char requestAudioKey[] = "GetNextKeyA";

  int result = -1;
  printf("Requesting new %s key\n", isVideo ? "video" : "audio");
  // don't send \0
  result = send(thisptr->sock, isVideo ? requestVideoKey : requestAudioKey, strlen(isVideo ? requestVideoKey : requestAudioKey), 0);

  if (result == -1) {
    perror("send() failed");
    exit(1);
  } else if (result != strlen(isVideo ? requestVideoKey : requestAudioKey)) {
    g_printerr("not everything sent\n");
    exit(1);
  }

  // callback will do the rest...
  if (isVideo) {
    thisptr->videoKeyConsumer = std::move(cb);
  } else {
    thisptr->audioKeyConsumer = std::move(cb);
  }
}

void Streamer::need_new_key_callback_video(__attribute__((unused)) GstElement *srtpenc, gpointer udata_this) {
  Streamer *thisptr = static_cast<Streamer *>(udata_this);
  need_new_key_callback(udata_this, true, std::bind(&Streamer::setNewKey, thisptr, std::placeholders::_1, std::placeholders::_2));
}
void Streamer::need_new_key_callback_audio(__attribute__((unused)) GstElement *srtpenc, gpointer udata_this) {
  Streamer *thisptr = static_cast<Streamer *>(udata_this);
  need_new_key_callback(udata_this, false, std::bind(&Streamer::setNewKey, thisptr, std::placeholders::_1, std::placeholders::_2));
}

/**
 * @brief creates a SRTP encrypt element. Caller has ownership
 */
GstElement *Streamer::createSRTPenc() {
  GstElement *srtpenc = gst_element_factory_make("srtpenc", nullptr);
  const char cipher[] = "aes-256-icm";
  const char authentication[] = "hmac-sha1-80";
  gst_util_set_object_arg((GObject *)srtpenc, "rtp-auth", authentication);
  gst_util_set_object_arg((GObject *)srtpenc, "rtp-cipher", cipher);
  gst_util_set_object_arg((GObject *)srtpenc, "rtcp-auth", authentication);
  gst_util_set_object_arg((GObject *)srtpenc, "rtcp-cipher", cipher);
  return srtpenc;
}

int Streamer::setupAudioPipeline() {
  GstElement *audiosrc = gst_element_factory_make("autoaudiosrc", nullptr);
  // On startup, srtpenc needs some time to initialize, so queue the data
  GstElement *queue = gst_element_factory_make("queue", nullptr);
  if (!audiosrc || !queue) {
    g_printerr("Failed to create audiosrc/queue\n");
    return 1;
  }
  g_object_set(queue, "leaky", 2, NULL);  // GST_QUEUE_LEAK_DOWNSTREAM = 2

  GstElement *audioconvert = gst_element_factory_make("audioconvert", nullptr);
  GstElement *rtpL16pay = gst_element_factory_make("rtpL16pay", nullptr);  // chooses ssrc automatically

  audioSRTPenc = createSRTPenc();
  audioUdpSink = gst_element_factory_make("udpsink", nullptr);
  if (!audioconvert || !rtpL16pay || !audioSRTPenc || !audioUdpSink) {
    g_printerr("Failed to create audioconvert/rtpL16pay/srtpenc/network\n");
    return 1;
  }
  g_object_set(rtpL16pay, "pt", 96, NULL);  // not strictly required, but being explicit about the payload type

  g_signal_connect(audioSRTPenc, "soft-limit", G_CALLBACK(&need_new_key_callback_audio), this);

  g_object_set(audioUdpSink, "host", dstHost.c_str(), "port", UDPAUDIOPORT, "sync", FALSE, "qos", TRUE, NULL);
  // pipeline takes ownership of elements
  gst_bin_add_many(GST_BIN(pipeline), audiosrc, queue, audioconvert, rtpL16pay, audioSRTPenc, audioUdpSink, NULL);

  if (gst_element_link_many(audiosrc, queue, audioconvert, NULL) != TRUE) {
    g_printerr("Failed to link audio audiosrc");
    return 1;
  }

  GstCaps *caps = nullptr;
  caps = gst_caps_from_string("audio/x-raw, rate=44100, channels=2");
  // Some devices have 2 microphones, so use 2 channels. By specifying it here we stay compatible with
  // single-microphone devices. In this case, audioconvert will do upmixing
  if (gst_element_link_filtered(audioconvert, rtpL16pay, caps) != TRUE) {
    g_printerr("Failed to link autoaudiosrc\n");
    return 1;
  }
  gst_caps_unref(caps);

  if (gst_element_link_many(rtpL16pay, audioSRTPenc, audioUdpSink, NULL) != TRUE) {
    g_printerr("Failed to link audio pipeline\n");
    return 1;
  }

  return 0;
}

std::shared_ptr<std::vector<int> > Streamer::getSupportedFocusModes() {
  if (supportedFocusModes->size() > 0) {
    return supportedFocusModes;
  }

  if (camsrc && g_object_class_find_property(G_OBJECT_GET_CLASS(G_OBJECT(camsrc)), "supported-focus-modes")) {
    GVariant *focus_modes;
    g_object_get(G_OBJECT(camsrc), "supported-focus-modes", &focus_modes, NULL);

    if (focus_modes) {
      int focus_count = g_variant_n_children(focus_modes);

      for (int i = 0; i < focus_count; i++) {
        GVariant *mode = g_variant_get_child_value(focus_modes, i);
        supportedFocusModes->push_back(g_variant_get_int32(mode));
        g_variant_unref(mode);
      }
    }
  }
  return supportedFocusModes;
}

void Streamer::refocusCamera(int mode) {
  // check if "mode" is valid
  const std::shared_ptr<std::vector<int> > supportedFocusModes = getSupportedFocusModes();
  if (std::find(supportedFocusModes->begin(), supportedFocusModes->end(), mode) == supportedFocusModes->end()) {
    mode = GST_PHOTOGRAPHY_FOCUS_MODE_CONTINUOUS_EXTENDED;
  }

  g_object_set(camsrc, "focus-mode", GST_PHOTOGRAPHY_FOCUS_MODE_AUTO, NULL);  // first switch to "auto" to trigger change
  g_object_set(camsrc, "focus-mode", mode, NULL);
}

void Streamer::changeDestination(const std::string& dstHost) {
  this->dstHost = dstHost;
  if (state == StreamState::Standby) {  // TODO support change while streaming
    g_object_set(videoUdpSink, "host", dstHost.c_str(), NULL);
    g_object_set(audioUdpSink, "host", dstHost.c_str(), NULL);
  }
}

void Streamer::changeResolution(int width, int height) {
  videoWidth = width;
  videoHeight = height;

  if (state == StreamState::Standby) {
    return;
  }

  setState(StreamState::Reconfiguring);
  g_print("%s\n", __func__);

  // will be called when camera capture stopped
  g_signal_connect(camsrc, DROIDCAM_STOPPED, G_CALLBACK(&changeResolutionStep1), this);

  GstPad *pad = gst_element_get_static_pad(videoUdpSink, "sink");
  if (pad == NULL) {
    throw std::logic_error("BUG while using GStreamer");
  }
  gst_pad_add_probe(pad, GST_PAD_PROBE_TYPE_BLOCK_DOWNSTREAM, &changeResolutionStep2, this, NULL);
  gst_object_unref(pad);

  g_signal_emit_by_name(camsrc, "stop-capture", NULL);
}

void Streamer::changeResolutionStep1(GstElement *  /*vidsrc_unused*/, gpointer udata_this) {
  Streamer *thisptr = static_cast<Streamer *>(udata_this);
  // camera is now stopped - remove ourselves again
  int i = g_signal_handlers_disconnect_by_func(thisptr->camsrc, reinterpret_cast<void *>(&changeResolutionStep1), thisptr);
  if (i != 1) {
    g_printerr("%s: Failed to clean up temporary signal handler (removed %d)\n", __func__, i);
    exit(EXIT_FAILURE);
  }

  if (gst_element_set_state(thisptr->camsrc, GST_STATE_READY) == GST_STATE_CHANGE_FAILURE) {
    g_printerr("Failed to set camsrc to GST_STATE_READY");
  }
  if (gst_element_set_state(thisptr->vf_fakesink, GST_STATE_NULL) == GST_STATE_CHANGE_FAILURE) {
    g_printerr("Failed to set vf_fakesink to GST_STATE_NULL");
  }

  gst_element_unlink(thisptr->camsrc, thisptr->vf_fakesink);
  gst_bin_remove(GST_BIN(thisptr->pipeline), thisptr->vf_fakesink);
  thisptr->vf_fakesink = gst_element_factory_make("fakesink", nullptr);
  gst_bin_add(GST_BIN(thisptr->pipeline), thisptr->vf_fakesink);

  GstCaps *caps;
  caps = gst_caps_new_simple("video/x-h264",
    "width", G_TYPE_INT, thisptr->videoWidth,
    "height", G_TYPE_INT, thisptr->videoHeight,
    "framerate", GST_TYPE_FRACTION, 30, 1,
    "stream-format", G_TYPE_STRING, "avc",
    "alignment", G_TYPE_STRING, "au",
    NULL);
  g_object_set(thisptr->videoCapsfilter, "caps", caps, NULL);
  gst_caps_unref(caps);

  if (gst_element_link_pads(thisptr->camsrc, "vfsrc", thisptr->vf_fakesink, "sink") != TRUE) {
    g_printerr("Failed to link fakesink");
    exit(EXIT_FAILURE);
  }
  if (gst_element_set_state(thisptr->camsrc, GST_STATE_PLAYING) == GST_STATE_CHANGE_FAILURE) {
    g_printerr("Failed to set camsrc to GST_STATE_PLAYING");
  }
  if (gst_element_set_state(thisptr->pipeline, GST_STATE_PLAYING) == GST_STATE_CHANGE_FAILURE) {
    g_printerr("Failed to set pipeline to GST_STATE_PLAYING");
  }

  start_camera_cb(thisptr);
}

GstPadProbeReturn Streamer::changeResolutionStep2(GstPad *pad, GstPadProbeInfo *info, gpointer  /*udata_this*/) {
  if (GST_EVENT_TYPE(GST_PAD_PROBE_INFO_DATA(info)) != GST_EVENT_EOS) {
    return GST_PAD_PROBE_PASS;
  }
  gst_pad_remove_probe(pad, GST_PAD_PROBE_INFO_ID(info));
  return GST_PAD_PROBE_DROP;  // catch EOS so that not the whole pipeline goes end of stream
}

GstElement *Streamer::createNewDroidcamsrc() {
  GstElement *cam = gst_element_factory_make("droidcamsrc", nullptr);
  if (!cam) {
    g_printerr("Failed to create video source\n");
    return nullptr;
  }

  gst_element_add_property_notify_watch(cam, "ready-for-capture", TRUE);  // send us messages on the bus

  g_object_set(cam, "mode", 2, NULL);          // mode: Capture mode (image or video) Enum "GstCameraBin2Mode"
  g_object_set(cam, "camera-device", 0, NULL); // (0): primary,  (1): secondary
  g_object_set(cam, "target-bitrate", 10 * 1024 * 1024, NULL);
  g_object_set(cam, "focus-mode", 6, NULL);  // 7 continuous-extended
  // 3 infinity  (4): hyperfocal  (5): extended (2): portrait (7): continuous-extended
  // see https://github.com/GStreamer/gstreamer/blob/master/tools/gst-inspect.c#L603 for how to query valid value range
  return cam;
}

bool Streamer::linkVideosrc() {
  // ! connecting the viewfinder source is important, otherwise it will stop due to pushing a buffer into an unlinked pad !
  // Apparently everything is designed for a mandatory use of a viewfinder...
  if (gst_element_link_pads(camsrc, "vfsrc", vf_fakesink, "sink") != TRUE) {
    g_printerr("Failed to link fakesink");
    return false;
  }

  GstCaps *caps;
  caps = gst_caps_new_simple("video/x-h264",
    "width", G_TYPE_INT, videoWidth,
    "height", G_TYPE_INT, videoHeight,
    "framerate", GST_TYPE_FRACTION, 30, 1,
    "stream-format", G_TYPE_STRING, "avc",
    "alignment", G_TYPE_STRING, "au",
    NULL);
  // caps = gst_caps_from_string("video/x-h264, width=1280, height=720, framerate=24/1");   // width=640, height=480
  g_object_set(videoCapsfilter, "caps", caps, NULL);
  gst_caps_unref(caps);

  if (gst_element_link_pads(camsrc, "vidsrc", videoCapsfilter, "sink") != TRUE) {
    g_printerr("Failed to link vidsrc\n");
    return false;
  }

  return true;
}

int Streamer::setupVideoPipeline() {
  camsrc = createNewDroidcamsrc();
  videoCapsfilter = gst_element_factory_make("capsfilter", nullptr);
  GstElement *videoSrcQueue = gst_element_factory_make("queue", nullptr);
  vf_fakesink = gst_element_factory_make("fakesink", nullptr);
  GstElement *rtppay = gst_element_factory_make("rtph264pay", nullptr);

  videoSRTPenc = createSRTPenc();
  videoUdpSink = gst_element_factory_make("udpsink", nullptr);
  if (!videoCapsfilter || !videoSrcQueue ||  !vf_fakesink || !rtppay || !videoSRTPenc || !videoUdpSink) {
    g_printerr("Failed to create other video elements\n");
    return 1;
  }

  gst_util_set_object_arg((GObject *)videoCapsfilter, "caps-change-mode", "delayed"); // Temporarily accept previous filter caps
  g_object_set(videoSrcQueue, "leaky", 2, NULL);                                      // GST_QUEUE_LEAK_DOWNSTREAM = 2

  /** (rtpmp4vpay)/MPEG-4 is too slow on Xperia X. H264 gives good latency. Afaik droidvenc was also good with H264.
   */

  g_object_set(rtppay, "config-interval", 2, NULL);
  g_object_set(rtppay, "pt", 96, NULL);  // not strictly required, but being explicit about the payload type

  g_signal_connect(videoSRTPenc, "soft-limit", G_CALLBACK(&need_new_key_callback_video), this);

  g_object_set(videoUdpSink, "host", dstHost.c_str(), "port", UDPVIDEOPORT, "sync", FALSE, "qos", TRUE, NULL);
  // pipeline takes ownership of elements
  gst_bin_add_many(GST_BIN(pipeline), camsrc, vf_fakesink, videoCapsfilter, videoSrcQueue, rtppay, videoSRTPenc, videoUdpSink, NULL);

  if (!linkVideosrc()) {
    g_printerr("Failed to link droidcamsrc\n");
    return 1;
  }

  if (gst_element_link_many( videoCapsfilter, videoSrcQueue, rtppay, NULL) != TRUE) {
    g_printerr("Failed to link queue\n");
    return 1;
  }

  GstPad *payloader_src, *enc_sink;
  payloader_src = gst_element_get_static_pad(rtppay, "src");
  enc_sink = gst_element_get_request_pad(videoSRTPenc, "rtp_sink_%u");
  if (gst_pad_link(payloader_src, enc_sink) != GST_PAD_LINK_OK) {
    g_printerr("Failed to link rtppay\n");
    return -1;
  }
  gst_object_unref(payloader_src);
  gst_object_unref(enc_sink);

  if (gst_element_link_many(videoSRTPenc, videoUdpSink, NULL) != TRUE) {
    g_printerr("Failed to link video network\n");
    return 1;
  }
  return 0;
}
