#include "settings.hpp"
#include <QDebug>

Settings::Settings(QObject *parent) : QObject(parent) {

}

bool Settings::getUseMki() const {
  return s.value(SK_UseMki, false).toBool();
}

void Settings::setDestinationHost(const QString &destHost) {
  bool changed = false;
  if (destHost != getDestinationHost()) {
    changed = true;
  }
  s.setValue(SK_DestHost, destHost);
  if (changed) {
    emit destinationHostChanged();
  }
}

QString Settings::getDestinationHost() const {
  return s.value(SK_DestHost, QStringLiteral("192.168.2.12")).toString();
}

void Settings::setVideoResolution(const QSize &resolution) {
  bool changed = false;
  if (resolution != getVideoResolution()) {
    changed = true;
  }
  s.setValue(SK_VideoResolution, resolution);
  if (changed) {
    emit videoResolutionChanged();
  }
}

QSize Settings::getVideoResolution()  const {
  return s.value(SK_VideoResolution, QSize()).toSize();
}
