#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#include <QObject>
#include <QSettings>
#include <QSize>

const QString SK_UseMki = QStringLiteral("disableGstreamerCompatibilityMode");
const QString SK_DestHost = QStringLiteral("destinationHost");
const QString SK_VideoResolution = QStringLiteral("videoResolution");

class Settings : public QObject {
  Q_OBJECT
  Q_PROPERTY(QString destinationHost READ getDestinationHost WRITE setDestinationHost NOTIFY destinationHostChanged)
  Q_PROPERTY(QSize videoResolution READ getVideoResolution WRITE setVideoResolution NOTIFY videoResolutionChanged)

  QSettings s;

  public:
  explicit Settings(QObject *parent = nullptr);

  QString getDestinationHost() const;
  void setDestinationHost(const QString &destHost);

  QSize getVideoResolution() const;
  void setVideoResolution(const QSize &res);

  Q_INVOKABLE bool getUseMki() const;

  signals:
  void destinationHostChanged();
  void videoResolutionChanged();

  public slots:
};

#endif  // SETTINGS_HPP
