#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif

#include "streamermodel.hpp"  // needs to be defined here because its macros have side effects on gstreamer
#include <sailfishapp.h>
#include <gst/gst.h>

GST_DEBUG_CATEGORY_STATIC(sailstream);
#define GST_CAT_DEFAULT sailstream

#include <QObject>
#include <QTimer>
#include <QCommandLineParser>
#include <QtDBus>
#include <QDBusMetaType>
#include <QDBusInterface>
#include <QDBusConnection>
#include <QDBusVariant>
#include <iostream>
#include <memory>

#include <getopt.h>

#include "resolutionquery.hpp"
#include "streamstateqt.hpp"
#include "settings.hpp"
#include "streamermodel-dbusadaptor.hpp"

const QString dbusService = QStringLiteral("org.harbour.sailstream");
const QString dbusPath = QStringLiteral("/org/harbour/sailstreamv0");

int runGUI(int *argc, char **argv[]) {
  // SailfishApp::main() will display "qml/harbour-sailstream.qml", if you need more
  // control over initialization, you can use:
  //
  //   - SailfishApp::application(int, char *[]) to get the QGuiApplication *
  //   - SailfishApp::createView() to get a new QQuickView * instance
  //   - SailfishApp::pathTo(QString) to get a QUrl to a resource file
  //   - SailfishApp::pathToMainQml() to get a QUrl to the main QML file
  //
  // To display the view, call "show()" (will show fullscreen on device).
  gst_init(argc, argv);
  GST_DEBUG_CATEGORY_INIT(sailstream, "SailStream", 0, "SailStream debug output");

  std::unique_ptr<QGuiApplication> application(SailfishApp::application(*argc, *argv));
  application->setApplicationVersion(QStringLiteral("0.1.1"));

  QCommandLineParser parser;
  parser.setApplicationDescription(QStringLiteral("Use a Sailfish device as external camera/microphone\n\n\
###\n\
  Pass '-c' as FIRST argument to use the control mode of SailStream.\n\
  It has its own --help menu.\n\
###"));
  parser.addHelpOption();
  parser.addVersionOption();
  parser.process(*application);

  qRegisterMetaType<Resolutions>("Resolutions");
  qmlRegisterUncreatableType<StreamStateQt>("harbour.sailstream.StreamState", 1, 0, "StreamState", QStringLiteral("Uncreatable type"));

  int result;
  {  // introduce new scope so that our StreamerModel is destroyed before gst_deinit()
    std::unique_ptr<QQuickView> view(SailfishApp::createView());

    ResolutionQuery q;
    view->rootContext()->setContextProperty(QStringLiteral("resolutionQuery"), &q);

    Settings settings;
    StreamerModel streamerModel{settings.getUseMki(), settings.getDestinationHost()};
    view->rootContext()->setContextProperty(QStringLiteral("settings"), &settings);
    view->rootContext()->setContextProperty(QStringLiteral("streamerModel"), &streamerModel);

    // this is weird, but QT's way: https://doc.qt.io/archives/qt-5.10/qdbusadaptorexample.html
    new StreamerModelDBusAdaptor(&streamerModel);
    if (!QDBusConnection::sessionBus().registerObject(dbusPath, &streamerModel)) {
      std::cerr << "Could not register to D-Bus. Remote control won't function!" << std::endl;
    }
    if (!QDBusConnection::sessionBus().registerService(dbusService)) {
      std::cerr << QDBusConnection::sessionBus().lastError().message().toStdString() << std::endl;
    }

    view->setSource(SailfishApp::pathTo(QStringLiteral("qml/harbour-sailstream.qml")));
    QObject::connect(view->engine(), &QQmlEngine::quit, &streamerModel, &StreamerModel::stop);

    view->show();
    result = application->exec();
  }
  gst_deinit();
  return result;
}

int runCLI(int *argc, char **argv[]) {
  // no QCommandLineParser because it can't handle optional arguments
  int c;

  bool refocusCam = false;
  bool listFocusModes = false;
  int focus_mode = -1;

  static struct option long_options[] =
  {
    {"help", no_argument, 0, 'h'},
    {"listFocusModes", no_argument, 0, 'l'},
    {"focus", optional_argument, 0, 'f'},
    {0, 0, 0, 0}
  };

  while ((c = getopt_long(*argc, *argv, "hlf::", long_options, NULL)) != -1) {
    switch (c) {
     case 'l':
       listFocusModes = true;
       break;
     case 'f':
       if (optarg) {
         focus_mode = std::atoi(optarg);
       }
       refocusCam = true;
       break;
     default:
       std::cout << "Control a running instance of SailStream via terminal" << std::endl << std::endl;
       std::cout << "-l, --listFocusModes: list all allowed FMODEs as well as their meaning" << std::endl;
       std::cout << "-f[FMODE], --focus=[FMODE]: refocus camera, optionally using mode FMODE (numeric value)" << std::endl;
       if (c == 'h') {
         return EXIT_SUCCESS;
       }
       return EXIT_FAILURE;
    }
  }

  QDBusInterface dbus(dbusService, dbusPath, QStringLiteral(DBUSINTERFACENAME), QDBusConnection::sessionBus());
  if (!dbus.isValid()) {
    std::cerr << "Could not connect to D-Bus: ";
    if (QDBusConnection::sessionBus().lastError().type() == QDBusError::ServiceUnknown) {
      std::cerr << "The app is not running";
    } else {
      std::cerr << QDBusConnection::sessionBus().lastError().message().toStdString() << std::endl;
    }
    return EXIT_FAILURE;
  }

  if (refocusCam) {
    QVariant arg;
    arg.setValue(focus_mode);
    dbus.call(QStringLiteral("refocusCamera"), arg);
  }

  if (listFocusModes) {
    QDBusMessage reply = dbus.call(QStringLiteral("getSupportedFocusModes"));
    if (reply.arguments().length() == 0 || reply.type() != QDBusMessage::ReplyMessage) {
      std::cerr << "Error receiving reply: " << reply.errorMessage().toStdString() << std::endl;
      return EXIT_FAILURE;
    }

    QDBusArgument dbusVariant = qvariant_cast<QDBusArgument>(reply.arguments().at(0));
    QVector<int> supportedFocusModes = qdbus_cast<QVector<int> >(dbusVariant);

    std::cout << "Available camera focus modes:" << std::endl;
    for (const int &mode: supportedFocusModes) {
      std::cout << "  " << mode << ": ";
      if (Streamer::focusModeNames.count(mode) >= 1) {
        std::cout << Streamer::focusModeNames.at(mode) << std::endl;
      } else {
        std::cout << "BUG: unknown mode" << std::endl;
      }
    }

    if (supportedFocusModes.length() == 0) {
      std::cout << "  Warning: Camera stream must be running to query allowed focus modes." << std::endl;
    }
  }

  return EXIT_SUCCESS;
}

int main(int argc, char *argv[]) {
  if (argc <= 0) {
    std::cout << "name expected at argv[0]" << std::endl;
    return EXIT_FAILURE;
  }

  if (argc >= 2) {
    if (std::string(argv[1]) == "-c") {
      argc--;
      argv[1] = argv[0];
      argv++;
      return runCLI(&argc, &argv);
    }
  }

  return runGUI(&argc, &argv);
}
