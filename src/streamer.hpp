#ifndef STREAMER_H
#define STREAMER_H

# define GLIB_VERSION_MIN_REQUIRED (GLIB_VERSION_CUR_STABLE)
# define GLIB_VERSION_MAX_ALLOWED (GLIB_VERSION_2_66)
#include <glib.h>
#include <gst/gst.h>
#define  GST_USE_UNSTABLE_API
#include <gst/interfaces/photography.h>

#include <functional>
#include <memory>
#include <map>

#define STR_EXPAND_IN_CALLER(token) #token
#define STR(token) STR_EXPAND_IN_CALLER(token)

#define TCPKEYSERVERPORT 5008
#define UDPVIDEOPORT 5004
#define UDPAUDIOPORT 5006

/** @brief Creates a GStreamer pipeline and streams the data. Requires a running GLib event loop to handle events.
 */
class Streamer {
  public:
  enum StreamState {Standby, Connecting, Connected, Streaming, Reconfiguring, Stopping, Stopped, Error};
  enum class MediaSelection {audio, video, audiovideo};

  static const std::map<int, std::string> focusModeNames;

  typedef std::function<void ()> StreamerStateCallback;
  typedef std::function<void (const std::string&)> ErrorCb;
  typedef std::function<void ()> FinishedCb;

  private:
  struct KeyMsg {
    char type;  // A or V
    uint8_t key[46];
    uint8_t mki[1];
  };
  typedef std::function<void (GstElement *, const struct KeyMsg *)> KeyMsgConsumer;

  int sock = -1;
  GstElement *pipeline{nullptr};

  /**
   * @brief video source, allows to change params like focus mode
   */
  GstElement *camsrc{nullptr};
  int videoWidth = 1280, videoHeight = 720;
  std::shared_ptr<std::vector<int> > supportedFocusModes;

  GstElement *vf_fakesink{nullptr};  // fake sink for viewfinder
  /**
   * @brief allows to change further video settings (resolution, ...)
   */
  GstElement *videoCapsfilter{nullptr};
  // GstElement *videoSrcQueue{nullptr};
  // GstCaps *pendingVideoCaps{nullptr};  // TODO allow live resolution changes - see commented methods below

  GstElement *audioSRTPenc{nullptr};
  GstElement *videoSRTPenc{nullptr};

  GstElement *videoUdpSink{nullptr};
  GstElement *audioUdpSink{nullptr};

  /** @brief If true, the receiver must run GStreamer version >= 1.16
   * If false, a null MKI is used. Use this for Gstreamer versions below 1.16
   *
   * Starting with 1.16, Gstreamer supports MKIs (Master Key Identifier)
   * See https://gstreamer.freedesktop.org/releases/1.16/
   */
  bool useMKI = true;
  MediaSelection ms;
  std::string dstHost;

  KeyMsgConsumer audioKeyConsumer;  // dynamic handler for onKeyDataReceived()
  KeyMsgConsumer videoKeyConsumer;

  StreamState state = StreamState::Standby;

  StreamerStateCallback onStatusChanged;
  ErrorCb onError;
  FinishedCb onFinished;

  void postErrorMsg(const std::string& msg);
  void setState(StreamState newState);
  static gboolean bus_msg_handler(__attribute__((unused)) GstBus *bus, GstMessage *msg, gpointer data);
  // static gboolean printResolutionInfo(gpointer user_data);

  static gboolean start_camera_cb(gpointer user_data);
  static gboolean stop_camera_cb(gpointer user_data);

  void connectKeyClient();
  static gboolean onConnectFinished(GIOChannel *source, GIOCondition condition, gpointer udata_this);
  static void startPipelines( GstElement *pipelineInstance, gpointer udata_this);

  // gconstpointer
  void setNewKey(GstElement *srtpenc, const struct KeyMsg *keyMsg);
  static gboolean onKeyDataReceived(GIOChannel *source, GIOCondition condition, gpointer udata_this);  // GIOFunc
  static void need_new_key_callback(gpointer udata_this, bool isVideo, KeyMsgConsumer cb);
  static void need_new_key_callback_video(GstElement *srtpenc, gpointer udata_this);
  static void need_new_key_callback_audio(GstElement *srtpenc, gpointer udata_this);

  static void changeResolutionStep1(GstElement *vidsrc_unused, gpointer udata_this);
  static GstPadProbeReturn changeResolutionStep2(GstPad *pad, GstPadProbeInfo *info, gpointer udata_this);

  void ensureSetupPipelines();
  static GstElement *createSRTPenc();
  int setupAudioPipeline();
  int setupVideoPipeline();
  static GstElement *createNewDroidcamsrc();
  bool linkVideosrc();

  public:

  /** Before using this class, gst_init(argc, argv) must be called (and optionally gst_deinit() for cleanup on exit)
   * @brief Streamer
   * @param useMKI
   * @param m
   * @param dstIpAddress
   */
  Streamer(bool useMKI, MediaSelection m, const std::string& dstHost, FinishedCb onFinished = FinishedCb{nullptr}, StreamerStateCallback onStatusChangedCallback = StreamerStateCallback(nullptr), ErrorCb onError = ErrorCb{nullptr});
  ~Streamer();

  /** Starts streaming. Blocks till end of stream.
   * @brief start
   * @return
   */
  void start();
  void stop();

  std::shared_ptr<std::vector<int> > getSupportedFocusModes();
  /** Updates the camera focus
   * @brief refocusCamera
   * @param mode the optinal mode to use
   */
  void refocusCamera(int mode = -1);
  void changeResolution(int width, int height);
  void changeDestination(const std::string& dstHost);

  StreamState getState() const;
};

#endif  // STREAMER_H
