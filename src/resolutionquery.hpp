#ifndef RESOLUTIONQUERY_HPP
#define RESOLUTIONQUERY_HPP

#include <QAbstractListModel>
#include <QObject>
#include <QCamera>
#include <set>
#include <memory>

struct Resolution {
  int width, height;
  Resolution(int width = 0, int height = 0) : width(width), height(height) {
  }
};

struct ResolutionCompare {
  bool operator ()(const Resolution& lhs, const Resolution& rhs) const {
    if (lhs.width == rhs.width) {
      return lhs.height < rhs.height;
    } else {
      return lhs.width < rhs.width;
    }
  }
};

typedef std::shared_ptr<std::set<Resolution, ResolutionCompare> > Resolutions;
Q_DECLARE_METATYPE(Resolutions);

/** We could also query the underlying hardware directly (libhardware?).
 * However, afaik gstdroidcamsrc uses the deprecated hardware/camera.h and not the current camera device HAL 3.2
 * (hardware/camera3.h), so we should fix that first...
 * @brief The ResolutionQuery class
 */
class ResolutionQuery : public QAbstractListModel {
  Q_OBJECT  // ! every file this thing is used MUST be a header and MUST be mentioned in the .pro's HEADERS
  Q_PROPERTY(int rowCount READ rowCount NOTIFY rowCountChanged)

  private:
  static const QString defaultMediaRecorderContainerFormat;
  QList<QSize> supportedResolutions;
  Resolutions cached;

  static bool qsizeGreaterThan(const QSize &a, const QSize &b);
  /**
   * @param deviceName of camera, e.g., "primary" or "secondary"
   * @return set of supported video resolutions
   */
  const Resolutions supportedVideoResolutions(const std::string& deviceName) const;

  public:
  ResolutionQuery();

  enum MyRoles {
    // Qt::DisplayRole/Qt::InitialSortOrderRole provided by Qt
    ResolutionValue = Qt::UserRole
  };

  virtual QHash<int, QByteArray> roleNames() const;
  virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
  virtual QVariant data(const QModelIndex &index, int role) const;

  // Q_INVOKABLE void setMediaRecorder(QObject *capture);
  Q_INVOKABLE QSize defaultResolution();

  /** @return the index of the given resolution in the current model or 0 if not found
   */
  Q_INVOKABLE uint indexOf(const QSize &resolution);
  Q_INVOKABLE bool isValidResolution(const QSize &resolution) const;
  Q_INVOKABLE static const QString getDefaultMediaRecorderContainerFormat(QObject *recorder);
  Q_INVOKABLE void init();

  /**
   * @return  set of supported video resolutions for the "primary" camera
   */
  Q_INVOKABLE const Resolutions supportedVideoResolutions();

  signals:
  void rowCountChanged();
};

#endif  // RESOLUTIONQUERY_HPP
