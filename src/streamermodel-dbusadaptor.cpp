#include "streamermodel-dbusadaptor.hpp"
#include <iostream>

StreamerModelDBusAdaptor::StreamerModelDBusAdaptor(StreamerModel *streamer) :  QDBusAbstractAdaptor(streamer), streamer(streamer) {
  qDBusRegisterMetaType<QVector<int> >();  // make QVector<int> available as DBus type (step 2/2)
}

void StreamerModelDBusAdaptor::refocusCamera(int mode) {
  streamer->refocusCamera(mode);
}

QVector<int> StreamerModelDBusAdaptor::getSupportedFocusModes() {
  std::shared_ptr<std::vector<int> > tmp = streamer->getSupportedFocusModes();
  return QVector<int>::fromStdVector(*tmp);
}
