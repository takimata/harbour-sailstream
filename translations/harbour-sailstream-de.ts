<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AboutPage</name>
    <message>
        <source>About SailStream</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Licensed under GNU GPLv3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Any contributions or code reviews are welcome!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Stream status</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ErrorDialog</name>
    <message>
        <source>An error occured</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please restart the application if the error persists.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Maybe SSH port forwarding is not set up?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FirstRead</name>
    <message>
        <source>Usage Instructions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prerequisites - This device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You must be able to SSH into this device (recommendation: set up key-based authentication).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prerequisites - Receiver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The host using this Sailfish device as a camera/microphone must have: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A script for receiving the stream and creating the virtual input devices is located at:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please copy it to the machine which should receive the stream and execute it. Its &apos;&lt;nobr&gt;&lt;tt&gt;--help&lt;/tt&gt;&lt;/nobr&gt;&apos; menu tells you how to proceed (in short: create virtual camera and setup SSH port forwarding).</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Forward cam + mic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Destination host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Destination IP or domain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Camera Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Connecting...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stream status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A bug occurred</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An error occured</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>It is possible that it hangs while saying &apos;Connected&apos;. This is due to the way how SSH port forwarding works and occurs when port forwarding is active but there is nobody listening on the other end. That is, if you see this, ensure that the receiver script is running.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Restarting camera not yet supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About SailStream</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>harbour-sailstream</name>
    <message>
        <source>Standby</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Connecting...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Streaming...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reconfiguring...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stopping...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stopped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A bug occurred</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
