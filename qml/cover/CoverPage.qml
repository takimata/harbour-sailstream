import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.sailstream.StreamState 1.0

CoverBackground {
  Image {

    source: "../../images/background.png" // svg not supported
    anchors {
      verticalCenter: parent.verticalCenter

      top: parent.top
      topMargin: Theme.paddingMedium

      right: parent.right
      rightMargin: Theme.paddingMedium
    }
    fillMode: Image.PreserveAspectFit
    opacity: 0.3
  }

  Column {
    anchors.verticalCenter: parent.verticalCenter
    width: parent.width

    Label {
      anchors.horizontalCenter: parent.horizontalCenter
      text: qsTr("Stream status") + ":"
    }

    Label {
      anchors.horizontalCenter: parent.horizontalCenter
      text: getStreamStateAsString(streamerModel.state)
    }
  }

  CoverActionList {
    id: coverAction
    enabled: getButtonEnabledFromStreamState(streamerModel.state)

    CoverAction {
      iconSource: getCoverActionIconFromStreamState(streamerModel.state)
      onTriggered: {
        switch (streamerModel.state) {
        case StreamState.Standby:
          streamerModel.start()
          break
        case StreamState.Connected:
        case StreamState.Streaming:
        case StreamState.Reconfiguring:
          streamerModel.stop()
          // case StreamState.Stopped // TODO
          break
        default:
          break
        }
      }
    }
  }

  function getCoverActionIconFromStreamState(state) {
    switch (state) {
    case StreamState.Standby:
      return "image://theme/icon-cover-next"
    case StreamState.Connecting:
    case StreamState.Connected:
    case StreamState.Streaming:
    case StreamState.Reconfiguring:
    case StreamState.Stopping:
    case StreamState.Stopped:
    case StreamState.Error:
      return "image://theme/icon-cover-pause"
    default:
      return ""
    }
  }
}
