import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
  id: page

  // The effective value will be restricted by ApplicationWindow.allowedOrientations
  allowedOrientations: Orientation.All

  SilicaFlickable {
    anchors.fill: parent
    contentHeight: content.height

    VerticalScrollDecorator {
    }

    Column {
      width: parent.width
      spacing: Theme.paddingMedium

      PageHeader {
        title: qsTr("About SailStream")
      }

      SectionHeader {
        text: qsTr("Source code")
      }

      LinkedLabel {
        color: Theme.highlightColor
        x: Theme.horizontalPageMargin
        width: parent.width - 2 * Theme.horizontalPageMargin
        wrapMode: Text.Wrap
        plainText: "https://gitlab.com/takimata/harbour-sailstream"
      }

      Label {
        color: Theme.highlightColor
        x: Theme.horizontalPageMargin
        width: parent.width - 2 * Theme.horizontalPageMargin
        wrapMode: Text.Wrap
        text: qsTr("Licensed under GNU GPLv3")
      }

      Label {
        color: Theme.highlightColor
        x: Theme.horizontalPageMargin
        width: parent.width - 2 * Theme.horizontalPageMargin
        wrapMode: Text.Wrap
        text: qsTr("Any contributions or code reviews are welcome!")
      }
    }
  }
}
