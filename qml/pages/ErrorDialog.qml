import QtQuick 2.2
import Sailfish.Silica 1.0

Dialog {
  property string message

  allowedOrientations: Orientation.All

  Column {
    width: parent.width
    spacing: Theme.paddingLarge

    DialogHeader {
      title: qsTr("An error occured")
    }

    Label {
      width: parent.width
      color: Theme.highlightColor
      x: Theme.horizontalPageMargin
      wrapMode: Text.Wrap
      text: message
    }

    Label {
      width: parent.width
      color: Theme.highlightColor
      x: Theme.horizontalPageMargin
      wrapMode: Text.Wrap
      text: qsTr("Maybe SSH port forwarding is not set up?")
    }

    Label {
      width: parent.width
      color: Theme.highlightColor
      x: Theme.horizontalPageMargin
      wrapMode: Text.Wrap
      text: qsTr("Please restart the application if the error persists.")
    }
  }

  onAccepted: {
    Qt.quit()
  }
}
