import QtQuick 2.5
import Sailfish.Silica 1.0

Page {
  property bool pageActive: status === PageStatus.Active

  allowedOrientations: Orientation.All

  onPageActiveChanged: {
    if (pageActive) {
      pageStack.pushAttached(mainPage)
    }
  }

  SilicaFlickable {
    anchors.fill: parent
    contentHeight: content.height
    VerticalScrollDecorator {
    }

    Column {
      id: content
      width: parent.width
      spacing: Theme.paddingSmall

      PageHeader {
        title: qsTr("Usage Instructions")
      }

      SectionHeader {
        text: qsTr("Prerequisites - This device")
      }

      Label {
        color: Theme.highlightColor
        x: Theme.horizontalPageMargin
        width: parent.width - 2 * Theme.horizontalPageMargin
        wrapMode: Text.Wrap
        text: qsTr(
                "You must be able to SSH into this device (recommendation: set up key-based authentication).")
      }

      SectionHeader {
        text: qsTr("Prerequisites - Receiver")
      }

      Label {
        color: Theme.highlightColor
        x: Theme.horizontalPageMargin
        width: parent.width - 2 * Theme.horizontalPageMargin
        wrapMode: Text.Wrap
        text: qsTr(
                "The host using this Sailfish device as a camera/microphone must have: ")
      }

      Label {
        color: Theme.highlightColor
        x: Theme.horizontalPageMargin
        width: parent.width - 2 * Theme.horizontalPageMargin

        wrapMode: Text.Wrap
        textFormat: Text.StyledText
        text: '<ul><li>for the camera: <b>v4l2loopback</b> (see <a href="https://github.com/umlaeute/v4l2loopback">github.com/umlaeute/v4l2loopback</a> if necessary)</li>
<li>for the microphone: <b>PulseAudio</b> (used by default in almost every Linux distribution)</li>
</ul>'
      }

      Label {
        color: Theme.highlightColor
        x: Theme.horizontalPageMargin
        width: parent.width - 2 * Theme.horizontalPageMargin
        wrapMode: Text.Wrap
        text: qsTr(
                "A script for receiving the stream and creating the virtual input devices is located at:")
      }

      TextArea {
        width: parent.width
        //readOnly: true // if true, copying is not possible
        text: "/usr/share/harbour-sailstream/sailstream-recv.py"
        labelVisible: false
        onClicked: {
          selectAll()
        }
        onTextChanged: {
          text = "/usr/share/harbour-sailstream/sailstream-recv.py"
        }
      }

      Label {
        color: Theme.highlightColor
        x: Theme.horizontalPageMargin
        width: parent.width - 2 * Theme.horizontalPageMargin
        wrapMode: Text.Wrap
        textFormat: Text.RichText
        fontSizeMode: "FixedSize"
        // DejaVu Sans Mono courier <span><font face='monospace'
        text: qsTr(
                "Please copy it to the machine which should receive the stream and execute it. Its '<nobr><tt>--help</tt></nobr>' menu tells you how to proceed (in short: create virtual camera and setup SSH port forwarding).") // <pre> unfortunately adds linebreaks
      }
    }
  }
}
