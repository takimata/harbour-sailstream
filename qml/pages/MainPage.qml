import QtQuick 2.0
import Sailfish.Silica 1.0
import QtMultimedia 5.6
import harbour.sailstream.StreamState 1.0

Page {
  allowedOrientations: Orientation.All

  SilicaFlickable {
    anchors.fill: parent

    PullDownMenu {
      MenuItem {
        text: qsTr("About SailStream")
        onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
      }
    }

    // having this camera here is mandatory to query QCameraInfo::availableCameras() in C++ (WTF?)
    //    Camera {
    //      id: camera

    //      cameraState: Camera.ActiveState
    //      captureMode: Camera.CaptureVideo
    //      deviceId: resolutionQuery.getDeviceID()

    //      videoRecorder {
    //        muted: true
    //        videoCodec: "video/x-h264"
    //        frameRate: 30
    //        mediaContainer: resolutionQuery.getDefaultMediaRecorderContainerFormat(
    //                          camera.videoRecorder)
    //        videoEncodingMode: CameraRecorder.AverageBitRateEncoding
    //      }

    //      onCameraStatusChanged: {

    //        //        if (cameraStatus === Camera.StartingStatus) {
    //        //          console.log("setting cam recorder")
    //        //          resolutionQuery.setMediaRecorder(camera.videoRecorder)
    //        //          camera.stop()
    //        //        }
    //      }
    //      Component.onCompleted: {
    //        resolutionQuery.setMediaRecorder(...)
    //        camera.cameraState = Camera.UnloadedState
    //      }
    //    }
    Column {
      anchors.fill: parent
      spacing: Theme.paddingSmall

      PageHeader {
        title: qsTr("Forward cam + mic")
      }

      TextField {
        width: parent.width
        text: settings.destinationHost
        EnterKey.enabled: text.length > 0
        placeholderText: qsTr("Destination IP or domain")
        label: qsTr("Destination host")
        enabled: streamerModel.state == StreamState.Standby
        onTextChanged: if (streamerModel.state == StreamState.Standby) {
                         settings.destinationHost = text
                         streamerModel.changeDestination(text)
                       }
      }
      SectionHeader {
        text: qsTr("Camera Settings")
      }

      ComboBox {
        id: listView2
        label: qsTr("Resolution")
        currentIndex: resolutionQuery.indexOf(settings.videoResolution)
        // TODO Changing resolution while streaming works at the sender side but only somewhat at the receiver...
        enabled: streamerModel.state == StreamState.Standby
        menu: ContextMenu {
          Repeater {
            model: resolutionQuery
            delegate: MenuItem {
              text: display

              onClicked: {
                console.log("selected resolution ", value)
                settings.videoResolution = value
                if (streamerModel.state == StreamState.Standby
                    || streamerModel.state == StreamState.Streaming) {
                  streamerModel.changeResolution(value.width, value.height)
                }
              }
            }
          }
        }
      }

      SectionHeader {
        text: qsTr("Stream status") + ": " + window.getStreamStateAsString(
                streamerModel.state)
      }

      Button {
        text: getButtonTextFromStreamState(streamerModel.state)
        x: Theme.horizontalPageMargin
        width: parent.width - 2 * Theme.horizontalPageMargin
        enabled: window.getButtonEnabledFromStreamState(streamerModel.state)
        preferredWidth: Theme.buttonWidthLarge
        onClicked: {
          switch (streamerModel.state) {
          case StreamState.Standby:
            streamerModel.start()
            break
          case StreamState.Connected:
          case StreamState.Streaming:
          case StreamState.Reconfiguring:
            streamerModel.stop()
            // case StreamState.Stopped // TODO
            break
          default:
            break
          }
        }
      }

      Label {
        color: Theme.highlightColor
        x: Theme.horizontalPageMargin
        width: parent.width - 2 * Theme.horizontalPageMargin
        wrapMode: Text.Wrap
        visible: streamerModel.state == StreamState.Connecting
                 || streamerModel.state == StreamState.Connected
        text: qsTr(
                "It is possible that it hangs while saying 'Connected'. This is due to the way how SSH port forwarding works and occurs when port forwarding is active but there is nobody listening on the other end. That is, if you see this, ensure that the receiver script is running.")
      }
    }
  }
  function currentResolution() {
    var res = resolutionQuery.defaultResolution()
    if (resolutionQuery.isValidResolution(res)) {
      return res
    }
    return Qt.size(0, 0)
  }

  function getButtonTextFromStreamState(state) {
    switch (state) {
    case StreamState.Standby:
      return qsTr("Start")
    case StreamState.Connecting:
      return qsTr("Connecting...")
    case StreamState.Connected:
    case StreamState.Streaming:
    case StreamState.Reconfiguring:
    case StreamState.Stopping:
      return qsTr("Stop")
    case StreamState.Stopped:
      return qsTr("Restarting camera not yet supported")
    case StreamState.Error:
      return qsTr("An error occured")
    default:
      return qsTr("A bug occurred")
    }
  }
}
