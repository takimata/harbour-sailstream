import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.sailstream.StreamState 1.0
import "pages"

ApplicationWindow {
  id: window
  property bool _showingError: false

  ErrorDialog {
    id: errorDialog
    onAccepted: _showingError = false
    onRejected: _showingError = false
  }

  function postError(errorDesc) {
    if (!_showingError) {
      _showingError = true
      errorDialog.message = errorDesc
      pageStack.push(errorDialog)
    }
  }

  initialPage: firstRead

  FirstRead {
    id: firstRead
  }

  MainPage {
    id: mainPage
  }

  Component.onCompleted: {
    resolutionQuery.init()
    streamerModel.errorMsg.connect(postError)
  }

  cover: Qt.resolvedUrl("cover/CoverPage.qml")
  allowedOrientations: defaultAllowedOrientations

  function getStreamStateAsString(state) {
    switch (state) {
    case StreamState.Standby:
      return qsTr("Standby")
    case StreamState.Connecting:
      return qsTr("Connecting...")
    case StreamState.Connected:
      return qsTr("Connected")
    case StreamState.Streaming:
      return qsTr("Streaming...")
    case StreamState.Reconfiguring:
      return qsTr("Reconfiguring...")
    case StreamState.Stopping:
      return qsTr("Stopping...")
    case StreamState.Stopped:
      return qsTr("Stopped")
    case StreamState.Error:
      return qsTr("Error")
    default:
      return qsTr("A bug occurred")
    }
  }

  function getButtonEnabledFromStreamState(state) {
    switch (state) {
    case StreamState.Standby:
      return true
    case StreamState.Connecting:
      return false
    case StreamState.Connected:
    case StreamState.Streaming:
    case StreamState.Reconfiguring:
      return true
    case StreamState.Stopping:
      return false
    case StreamState.Stopped:
      return false
    case StreamState.Error:
      return false
    default:
      return false
    }
  }
}
