#!/usr/bin/env python3

import sys, os
import gi # python3-gst-1.0
gi.require_version('Gst', '1.0')
gi.require_version('GLib', '2.0')
gi.require_version('GObject', '2.0')
from gi.repository import GLib, GObject, Gst

import argparse, textwrap
import asyncio, threading
import socket
import signal
import subprocess, tempfile
import queue
import secrets
import numpy
import logging
from collections import namedtuple

SRTPKey = namedtuple('SRTPKey', 'key mki') # master key and associated identifier (both as bytes)

# This python version and the C version both have ~ 100 to 200 ms latency.
# Some of this accounts to moving the image into /dev/video0, but most to droidcamrc

from typing import Dict
def mergeDicts(a : Dict, b : Dict) -> Dict:
    temp = a.copy()
    temp.update(b)
    return temp

def clearQueue(q: queue.Queue):
    try:
        while True:
            _ = q.get_nowait()
    except queue.Empty:
        pass

def gstreamerSupportsMKI() -> bool:
    """ MKIs were added in Gstreamer 1.16 """
    if Gst.VERSION_MAJOR > 1 or (Gst.VERSION_MAJOR == 1 and Gst.VERSION_MINOR >= 16):
        return True
    else:
        return False

class SailCamReceiver():
    def __init__(self, debug: bool, tcpKeyserverPort: int, udpVideoPort:int, udpAudioPort:int, videoDevice: str):
        self.debug = debug
        logging.basicConfig(level=logging.DEBUG if debug else logging.WARNING, format="%(levelname)s:%(message)s")
        self.log = logging.getLogger("")
        
        self.videoSRTPdec = None
        self.audioSRTPdec = None
        
        self.pipeline = None
        self.apipeline = None # using 2 bins for audio/video didn't work - temp fix before moving to rtpbin...
        self.videoDevice = videoDevice
        self.tcpKeyserverPort = tcpKeyserverPort
        self.udpVideoPort = udpVideoPort
        self.udpAudioPort = udpAudioPort
        self.keyServerThread = None
        self.glib_loop = None
        
        self._server = None
        self._server_task = None
        
        self._connection = None # StreamWriter to connected client
        self._isShuttingDown = False
        
        self.useMki = gstreamerSupportsMKI()
        
        #  TODO look at https://gstreamer.freedesktop.org/documentation/rtpmanager/rtpbin.html#rtpbin 
        # to combine audio+video and avoid separate srtpdec
        # These 2 distinct pipelines are not nice...
        
        # Some sync between key server <-> gstreamer threads is needed for the keys:
        self.videoKeyGenLock = threading.Lock() # locks all related activity
        self.videoSRTPKeyQueue = queue.Queue(maxsize=1) # FIFO queue for 'SRTPKey's
        self.videoNextMki = numpy.uint8(0) # ensure consistent representation here and in C
        self.videoCurrentKeyData = None;
        
        self.audioKeyGenLock = threading.Lock()
        self.audioSRTPKeyQueue = queue.Queue(maxsize=1) # FIFO queue for 'SRTPKey's
        self.audioNextMki = numpy.uint8(0) # ensure consistent representation here and in C
        self.audioCurrentKeyData = None;
        #self.previousKeyData = None;
        
        
        # setting up Pulseaudio microphone
        self.audioFifoPath = os.path.join(tempfile.gettempdir(), "sailcam_input") # path to write audio to
        self.pulseaudioMicModuleID = None
        self.micDestroyLock = threading.Lock() # held while deleting mic
        # this may timeout sometimes, so retry a few times. Yes, the insane quoting on source_properties is necessary
        for i in range(0, 3):
            result = subprocess.run(["pactl", "load-module", "module-pipe-source", f"file='{self.audioFifoPath}'", 
                                    "format=s16be", "rate=44100", "channels=2", 
                                    'source_properties=\'device.description="Sailcam\ Virtual\ Microphone"\''], 
                                    capture_output=True, encoding="utf-8")
            if result.returncode == 0:
                self.pulseaudioMicModuleID = int(result.stdout.strip())
                break
            else:
                print(f"Error '{result.stderr.strip()}' while creating mic, retrying", file=sys.stderr)
        if self.pulseaudioMicModuleID is None:
            print(f"Error creating mic, exiting", file=sys.stderr)
            sys.exit(1)
    
    def bus_call(self, bus, message, loop):
        t = message.type
        if t == Gst.MessageType.EOS:
            self.log.debug("End-of-stream")
            loop.quit()
            self._remove_mic()
        elif t == Gst.MessageType.ERROR:
            err, dbg = message.parse_error()
            print(f"Got Error {err}: {dbg}", file=sys.stderr)
            loop.quit()
        return True
    
    def _getNextKey(self, isVideo: bool) -> SRTPKey:
        """ Retrieves the next key, mki pair or computes it if the other thread hasn't created it yet"""
        if isVideo:
            print("Video key requested")
            lock = self.videoKeyGenLock
            q = self.videoSRTPKeyQueue
            mki = self.videoNextMki
            self.videoNextMki += numpy.uint8(1) # ctypes' types can't be added...
        else:
            print("Audio key requested")
            lock = self.audioKeyGenLock
            q = self.audioSRTPKeyQueue
            mki = self.audioNextMki
            self.audioNextMki += numpy.uint8(1)
        
        with lock:
            try:
                return q.get(block=False)
            except queue.Empty:
                key = secrets.token_bytes(46)
                #key = b"0123456789012345678901234567890123456789012345"
                keydata = SRTPKey(key, mki.tobytes())
                q.put(keydata)
                return keydata
    
    def _serve_keys(self):
        _num_connection = 0
        async def handle_connection(reader, writer):
            nonlocal _num_connection
            _num_connection += 1 # no racecondition because we are in only one thread
            if _num_connection > 1:
                writer.close()
                print("Closing connection: max. 1 client allowed")
                return
            
            # emit signal to clear all keys on new connection. This should also reset roc
            self.audioSRTPdec.emit("clear-keys")
            self.videoSRTPdec.emit("clear-keys")
            self._connection = writer
            
            while True:
                try:
                    _ = await reader.readuntil(b"GetNextKey")
                    mediaType = await reader.readexactly(1)
                except asyncio.IncompleteReadError as e:
                    break
                
                keydata = self._getNextKey(mediaType == b"V")
                # TODO make proper protocol
                writer.write((b"V" if mediaType == b"V" else b"A") + keydata.key + keydata.mki)
                await writer.drain()
                print(f"Sent key {keydata.key} with mki = {keydata.mki}")

            _num_connection -= 1
            print(f"Closing socket; client probably disconnected; {_num_connection}")
            writer.close()
            clearQueue(self.videoSRTPKeyQueue)
            clearQueue(self.audioSRTPKeyQueue)
        
        async def serve():
            self._server = await asyncio.start_server(handle_connection, "localhost", self.tcpKeyserverPort, start_serving=True)
            print(f"Serving keys for SRTP on {self._server.sockets[0].getsockname()}")

            self._server.get_loop().set_debug(self.debug)
            
            async with self._server:
                self._server_task = self._server.get_loop().create_task(self._server.serve_forever())
                await self._server_task
                print("self._server_task finished")
            
            print("server finished")

        try:
            asyncio.run(serve())
        except asyncio.CancelledError as e:
            pass
        except RuntimeError as e:
            print("_serve_keys error:")
            print(e)
    
    def _startKeyServer(self):
        self.keyServerThread = threading.Thread(target=self._serve_keys)
        self.keyServerThread.start()
    
    def _stopKeyServer(self):
        self.log.debug("Stopping key server...")
        def shutdown():
            if self._connection is not None:
                self._connection.close()
            self._server.close()
        self._server.get_loop().call_soon_threadsafe(shutdown)
        self.keyServerThread.join() # wait till everything is closed
        self.log.debug("Stopping key server complete")
    
        
    def request_key_callback(self, isVideo:bool, gstsrtpdec, ssrc, user_data = None):
        """ https://gstreamer.freedesktop.org/documentation/srtp/srtpdec.html#srtpdec
        'Signal emitted to get the parameters relevant to stream with ssrc. 
        User should provide the key and the RTP and RTCP encryption ciphers and authentication, and return them wrapped in a GstCaps.'
        
        user_data is True for video and False for audio
        """
        if isVideo:
            previousKeyData = self.videoCurrentKeyData
            self.videoCurrentKeyData = self._getNextKey(True)
            currentKeyData = self.videoCurrentKeyData
        else:
            previousKeyData = self.audioCurrentKeyData
            self.audioCurrentKeyData = self._getNextKey(False)
            currentKeyData = self.audioCurrentKeyData
        
        cipher = "aes-256-icm" # MINOR: AES_256_GCM comes with Gstreamer 1.16
        authentication = "hmac-sha1-80"
        capsdict = {
                    "srtp-cipher": cipher,
                    "srtp-auth": authentication,
                    "srtcp-cipher": cipher,
                    "srtcp-auth": authentication,
                    "roc": 0,
                    "payload": 96,
                    "ssrc": numpy.int32(ssrc).item(), # GLib outputs unsigned ints but expects signed ints
                    }
        
        capsdict.update({"srtp-key": Gst.Buffer.new_wrapped(currentKeyData.key),
                        })
        if self.useMki:
            capsdict.update({"mki": Gst.Buffer.new_wrapped(currentKeyData.mki)})
        
        #if previousKeyData is not None:
        #    capsdict.update({"srtp-key2": Gst.Buffer.new_wrapped(previousKeyData.key),
        #                    })
        #    if self.useMki:
        #        capsdict.update({"mki2": Gst.Buffer.new_wrapped(previousKeyData.mki)})
        
        caps = Gst.Caps(Gst.Structure("application/x-srtp", **capsdict))
        print(f"Request keys for {'video' if isVideo else 'audio'} ssrc {ssrc}, returning {caps}")
        return caps
    
    def audio_request_key_callback(self, gstsrtpdec, ssrc, user_data = None):
        return self.request_key_callback(False, gstsrtpdec, ssrc, user_data)
    def video_request_key_callback(self, gstsrtpdec, ssrc, user_data = None):
        return self.request_key_callback(True, gstsrtpdec, ssrc, user_data)

    def on_decodebin_srcpad_added(self, decodebin, pad, *user_data):
        # Create/link rest of video pipeline here
        print("Adding videoconvert")
        videoconvert = Gst.ElementFactory.make("videoconvert")
        
        self.pipeline.add(videoconvert)
        linkres = decodebin.link(videoconvert)
        if not linkres:
            print(f"Could not link decodebin: {linkres}")
        
        v4l2sink = Gst.ElementFactory.make("v4l2sink")
        v4l2sink.set_property("sync", "false")
        v4l2sink.set_property("device", self.videoDevice)
        self.pipeline.add(v4l2sink)
            
        linkres = videoconvert.link(v4l2sink)
        if not linkres:
            print(f"Could not link videoconvert: {linkres}")

    def start(self, ip: str):
        self._startKeyServer()
        Gst.init(None)
        
        self.pipeline = Gst.Pipeline.new("pipeline_video")
        self.apipeline = Gst.Pipeline.new("pipeline_audio")
        
        def createUDPSource(isVideo: bool) -> Gst.Element:
            source = Gst.ElementFactory.make("udpsrc", "udpsrc_" + ('video' if isVideo else 'audio'))
            source.set_property("address", ip)
            source.set_property("port", self.udpVideoPort if isVideo else self.udpAudioPort)
            source.set_property("caps", Gst.Caps(Gst.Structure('application/x-srtp',
                                                                  **{"payload": 96,
                                                                     #"ssrc":2147483647,
                                                                     })))
            return source
        
        videoSource = createUDPSource(True)
        audioSource = createUDPSource(False)

        videoQueue = Gst.ElementFactory.make("queue", "queue_video")
        audioQueue = Gst.ElementFactory.make("queue", "queue_audio")
        videoQueue.set_property("leaky", "downstream")
        audioQueue.set_property("leaky", "downstream")


        def createSRTPdec(callback) -> Gst.Element:
            srtpdec = Gst.ElementFactory.make("srtpdec")
            srtpdec.connect("request-key", callback)
            srtpdec.connect("soft-limit", callback)
            srtpdec.connect("hard-limit", callback)
            return srtpdec
        
        self.videoSRTPdec = createSRTPdec(self.video_request_key_callback)
        self.audioSRTPdec = createSRTPdec(self.audio_request_key_callback)
        
        rtph264depay = Gst.ElementFactory.make("rtph264depay") # video
        rtpL16depay = Gst.ElementFactory.make("rtpL16depay") # raw audio
        
        decodebin = Gst.ElementFactory.make("decodebin")
        decodebin.connect("pad-added", self.on_decodebin_srcpad_added)
        # add/link the videoconvert and move-to-/dev/video part later
        
        self.pipeline.add(videoSource)
        self.pipeline.add(videoQueue)
        self.pipeline.add(self.videoSRTPdec)
        self.pipeline.add(rtph264depay)
        self.pipeline.add(decodebin)
        
        print("Adding audioconvert")
        audioconvert = Gst.ElementFactory.make("audioconvert")
        audiosink = Gst.ElementFactory.make("filesink")
        audiosink.set_property("sync", True)
        audiosink.set_property("location", self.audioFifoPath)
        
        self.apipeline.add(audioSource)
        self.apipeline.add(audioQueue)
        self.apipeline.add(self.audioSRTPdec)
        self.apipeline.add(rtpL16depay)
        self.apipeline.add(audioconvert)
        self.apipeline.add(audiosink)
        
        def link(a, b):
            linkres = a.link(b)
            if not linkres:
                print(f"Could not link elements {a}, {b}: {linkres}", file=sys.stderr)
                
        linkres = videoSource.link(self.videoSRTPdec)
        if not linkres:
            print(f"Could not link source: {linkres}", file=sys.stderr)
        
        link(self.videoSRTPdec, rtph264depay)
        link(rtph264depay, decodebin)
        
        # audio
        link(audioSource, audioQueue)
        link(audioQueue, self.audioSRTPdec)
        caps = Gst.Caps(Gst.Structure("application/x-rtp", **{
            "clock-rate": 44100,
            "channels": 2,
            }))
        linkres = self.audioSRTPdec.link_filtered(rtpL16depay, caps)
        if not linkres:
            print(f"Could not link audioSRTPdec: {linkres}", file=sys.stderr)
        
        link(rtpL16depay, audioconvert)
        
        caps = Gst.Caps(Gst.Structure("audio/x-raw", **{ # strictly speaking, converting should not be necessary
            "format": "S16BE", 
            "rate": 44100,
            "layout": "interleaved",
            "channels": 2,
            }))
        linkres = audioconvert.link_filtered(audiosink, caps)
        if not linkres:
            print(f"Could not link rtpL16depay: {linkres}", file=sys.stderr)
        

        self.glib_loop = GLib.MainLoop()
        self.pipeline.set_state(Gst.State.PLAYING)
        self.apipeline.set_state(Gst.State.PLAYING)

        bus = self.pipeline.get_bus()
        bus.add_signal_watch()
        bus.connect("message", self.bus_call, self.glib_loop)

        bus = self.apipeline.get_bus()
        bus.add_signal_watch()
        bus.connect("message", self.bus_call, self.glib_loop)

        try:
            self.glib_loop.run()
            self.log.debug("self.glib_loop.run() finished")
        except:
            pass

        self.stop()
    
    def _remove_mic(self): # this is called from the audio AND video pipeline
        with self.micDestroyLock:
            if self.pulseaudioMicModuleID is not None:
                for i in range(0, 3):
                    result = subprocess.run(["pactl", "unload-module", str(self.pulseaudioMicModuleID)], 
                                            capture_output=True, encoding="utf-8")
                    if result.returncode == 0:
                        self.pulseaudioMicModuleID = None
                        break
                    else:
                        print(f"Error {result.stderr} deleting mic, retrying", file=sys.stderr)
    
    def stop(self):
        # cleanup
        if not self._isShuttingDown:
            self._isShuttingDown = True
            if  (self.apipeline.get_state(0).state != Gst.State.PLAYING and \
                 self.pipeline.get_state(0).state != Gst.State.PLAYING) or not self.glib_loop.is_running():
                # if the event loop is not running or pipeline not ready for events => EOS won't be handled
                self.log.debug("Forcing shutdown")
                self._remove_mic()
                self.glib_loop.quit()
            else:
                self.pipeline.send_event(Gst.Event.new_eos())
                self.apipeline.send_event(Gst.Event.new_eos())
            self._stopKeyServer()

if __name__ == '__main__':
    tcpKeyserverPort = 5008
    udpVideoPort = 5004
    udpAudioPort = 5006
    
    v4l2loopback_desc = textwrap.dedent("""
    Please create a virtual video device before starting this program. Use e.g.,

        sudo modprobe v4l2loopback exclusive_caps=1 card_label=\"Sailcam\"

    When you are done using SailStream, you may remove it with

        sudo modprobe -r v4l2loopback

    (or just reboot).
    v4l2loopback is available in the repositories of most major Linux distributions
    and at https://github.com/umlaeute/v4l2loopback""")
    
    class MyCombinedFormatter(argparse.RawDescriptionHelpFormatter, argparse.ArgumentDefaultsHelpFormatter): pass
    parser = argparse.ArgumentParser(formatter_class=MyCombinedFormatter,
                                     description=textwrap.dedent(f"""\
                                     This is the receiver part of SailStream.
                                     It requires PulseAudio for the sound and an existing v4l2loopback device as virtual camera:
                                        audio stream --> virtual PulseAudio microphone (automatically created)
                                        video stream --> v4l2loopback device (created by you - see below)
                                     Any application can then access the stream via these virtual devices.

                                     \n
                                     """) + v4l2loopback_desc + textwrap.dedent(
                                     f"""\n
                                     Since the video/audio stream is encrypted and authenticated, the sender and receiver need to negotiate a 
                                     shared key. For security reasons, this is done via localhost:{tcpKeyserverPort}. Set up SSH port forwarding
                                     to allow the SailStream app to connect to this script:

                                        ssh -R {tcpKeyserverPort}:localhost:{tcpKeyserverPort} Sailfish
                                     """))
    parser.add_argument("--debug", "-d", default=False, action="store_true", help="Enable debugging")
    parser.add_argument("--listenIP", "-ip", default="192.168.2.4", type=str, help="IP to receive the SRTP stream on")
    parser.add_argument("--videoDevice", "-v", default="/dev/video0", type=str, help="Video device to use (already created by v4l2loopback)")
    
    args = parser.parse_args()
    
    if args.debug:
        import tracemalloc
        tracemalloc.start()
        import warnings
        warnings.simplefilter('always', ResourceWarning)
    
    try:
        stat = os.stat(args.videoDevice)
    except FileNotFoundError:
        print(f"Error: {args.videoDevice} does not exist", file=sys.stderr)
        print(v4l2loopback_desc, file=sys.stderr)
        sys.exit(1)
    # https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/Documentation/admin-guide/devices.txt#n1412
    sysfsPath = os.path.realpath( os.path.join(os.sep, "sys", "dev", "char", f"{os.major(stat.st_rdev)}:{os.minor(stat.st_rdev)}") )
    if not str(sysfsPath).startswith(os.path.join(os.sep, "sys", "devices", "virtual")):
        print(f"{args.videoDevice} is not a virtual device", file=sys.stderr)
        print(v4l2loopback_desc, file=sys.stderr)
        sys.exit(1)
    
    client = SailCamReceiver(args.debug, tcpKeyserverPort, udpVideoPort, udpAudioPort, args.videoDevice)

    for sig in (signal.SIGINT,): # , signal.SIGTERM
        signal.signal(sig, lambda signum, frame: client.stop())
    
    client.start(args.listenIP)
