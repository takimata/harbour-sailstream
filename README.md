This is the result of figuring out how to securely use the camera and microphone of a SailfishOS device as normal input devices on another computer.

It consists of a SailfishOS application which takes the data and wraps it into a SRTP stream (encrypted + integrity protected) which is then sent over the network.
The receiver runs a python script to decrypt and inject the stream into a virtual camera/microphone. The keys used to protect the data are transmitted via SSH port forwarding.

It currently requires PulseAudio and v4l2loopback on the receiver side.

# Usage
The UI should provide enough guidance. [`sailstream-recv.py --help`](receiver/sailstream-recv.py) details the receiver side.


If the audio is too quiet, you can adjust the volume of the virtual microphone with:

	export VOL=400 # or any other volume in %
	pacmd set-source-volume $SOURCE_INDEX $((VOL * 65536 / 100))

You can find the correct `$SOURCE_INDEX` with `pacmd list-sources` (look for `name: <fifo_input>`)

# Env for GStreamer debugging
    GST_TRACERS="latency(flags=pipeline+element)" GST_DEBUG="GST_TRACER:7"
    GST_DEBUG="droidcamsrc*:3,GST_CAPS:3,1"

# Discussion
on how to handle the data on the receiver side:

## Virtual camera
<https://github.com/umlaeute/v4l2loopback>

	sudo modprobe v4l2loopback exclusive_caps=1 card_label="Sailcam"

Firefox may only display a still image while Chromium requires `exclusive_caps=1` to work

## Virtual mic
### PulseAudio
### Standalone
Pulseaudio has a built-in way of sharing a mic, see <https://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/User/Modules/#module-native-protocol-unixtcp>.
Works and is stable enough, but has problems with connection breaks (it's TCP, after all).

Sender (real microphone):
	
	pacmd load-module module-native-protocol-tcp "auth-ip-acl=127.0.0.1;192.168.2.12/24"

Receiver (virtual microphone):

	pacmd load-module module-tunnel-source server=192.168.2.15:4713

Both must share the same auth-cookie.

#### Path: sailstream-recv.py -> Virtual Speaker -> Monitor of Virtual Speaker -> Virtual Source (= Mic)
    
    pacmd load-module module-null-sink sink_name=sailcam_output sink_properties='device.description="sailcam-recv.py\ Output"'
    pacmd load-module module-remap-source master=sailcam_output.monitor source_name=sailcam_mic source_properties='device.description="Sailcam\ Virtual\ Microphone"'

and feed it like this:

    gst-launch-1.0 filesrc location=test.wav ! wavparse ! audioconvert ! pulsesink device="sailcam_output"

#### Path: sailstream-recv.py -> named pipe -> Virtual Source (= Mic)
    
    pacmd load-module module-pipe-source file="/run/user/1000/sailcam_input" format=s16le rate=44100 channels=2 source_properties='device.description="Sailcam\ Virtual\ Microphone"'

and feed it like this:

    gst-launch-1.0 filesrc location=test.wav ! wavparse ! audioconvert ! 'audio/x-raw, format=(string)S16LE, layout=(string)interleaved, rate=(int)44100, channels=(int)2' ! filesink location="/run/user/1000/sailcam_input" sync=true
    
Sailstream is currently doing it this way since this is the simplest solution working on most Linux desktops.

@JACK/ALSA/OSS Users: Please get in touch if you have an idea how to do something similar on your sound system (preferably without loading any kernel modules and preferably compatible so that e.g., an ALSA solution is usable for Pulseaudio/JACK users, too)

### ALSA
would also be possible via

    sudo modprobe snd-aloop pcm_substreams=1 id="SailcamMic"
    gst-launch-1.0 filesrc location=test.wav ! wavparse ! audioconvert ! alsasink device="plughw:CARD=SailcamMic,DEV=1"

but: soundcard name "SailcamMic" not visible in pulseaudio => hard to select correct source/device on first try


# GStreamer pipelines for testing
to receive:

	gst-launch-1.0 -v udpsrc address=192.168.2.4 port=5004 caps='application/x-srtp, payload=(int)96, ssrc=(uint)2147483647, srtp-key=(buffer)30313233343536373839303132333435363738393031323334353637383930313233343536373839303132333435, srtp-cipher=(string)aes-256-icm, srtp-auth=(string)hmac-sha1-80, srtcp-cipher=(string)aes-256-icm, srtcp-auth=(string)hmac-sha1-80, roc=(uint)0' ! srtpdec ! rtph264depay ! decodebin ! videoconvert ! autovideosink


and to send:
    
    gst-launch-1.0 videotestsrc ! "video/x-raw,width=640,height=480" ! x264enc ! video/x-h264,stream-format=byte-stream ! rtph264pay ! 'application/x-rtp, payload=(int)96, ssrc=(uint)2147483647' ! srtpenc key=30313233343536373839303132333435363738393031323334353637383930313233343536373839303132333435 rtp-cipher="aes-256-icm" rtp-auth="hmac-sha1-80" rtcp-cipher="aes-256-icm" rtcp-auth="hmac-sha1-80" ! udpsink host=192.168.2.4 port=5004
    

or

    gst-launch-1.0 filesrc location=test.bin ! h264parse ! rtph264pay ssrc=2147483647 ! 'application/x-rtp, payload=(int)96' ! srtpenc key=30313233343536373839303132333435363738393031323334353637383930313233343536373839303132333435 rtp-cipher="aes-256-icm" rtp-auth="hmac-sha1-80" rtcp-cipher="aes-256-icm" rtcp-auth="hmac-sha1-80" ! udpsink host=192.168.2.4 port=5004


# Also see
<https://gstreamer.freedesktop.org/documentation/srtp/srtpenc.html#srtpenc>

<https://gstreamer.freedesktop.org/documentation/srtp/srtpdec.html#srtpdec>
