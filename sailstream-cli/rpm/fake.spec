## qmake needs this...

Name:       sailstream-cli

# >> macros
# turn off compiling python files  - "undefine _python_bytecompile_extra" doesn't work with this rpm version
%undefine __python
# << macros
%define source_date_epoch_from_changelog 1
%define _buildhost SailfishSDK
%define clamp_mtime_to_source_date_epoch 1

Summary:    Use a Sailfish device as webcam + microphone on another computer - Command line tool
Version:    0.1.1
Release:    1
Group:      Qt/Qt
License:    GPL-3.0
URL:        http://example.org/
Source0:    %{name}-%{version}.tar.bz2
Source100:  harbour-sailstream.yaml
Requires:   sailfishsilica-qt5 >= 0.10.9
Requires:   gstreamer1.0
Requires:   gstreamer1.0-droid
Requires:   gstreamer1.0-plugins-good
Requires:   gstreamer1.0-plugins-bad
BuildRequires:  pkgconfig(sailfishapp) >= 1.0.2
BuildRequires:  pkgconfig(Qt5Core)
BuildRequires:  pkgconfig(Qt5Qml)
BuildRequires:  pkgconfig(Qt5Quick)
BuildRequires:  pkgconfig(Qt5Multimedia)
BuildRequires:  pkgconfig(gstreamer-1.0)
BuildRequires:  desktop-file-utils

%description
Streams the camera and microphone data of your Sailfish device to your Linux computer and injects it into the multimedia system as a virtual camera and microphone.

%prep
%setup -q -n %{name}-%{version}

# >> setup
# << setup

%build
# >> build pre
# << build pre

%qmake5 

make %{?_smp_mflags}

# >> build post
# << build post

%install
rm -rf %{buildroot}
# >> install pre
# << install pre
%qmake5_install

# >> install post
# << install post

%files
%defattr(-,root,root,-)
%{_bindir}
# >> files
# << files
