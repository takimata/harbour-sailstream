TEMPLATE = app
TARGET = sailstream-cli
CONFIG += console
CONFIG -= qt

#CONFIG += c++17 # doesn't work with current Qt
QMAKE_CXXFLAGS += -std=c++17

# setting this include path manually seems to be necessary for <cstdlib> (currently)
QMAKE_CXXFLAGS += -Wall -Wextra -I/usr/include/c++/8.3.0/

HEADERS += \
    ../src/streamer.hpp
INCLUDEPATH += ../src/
SOURCES += \
    main.cpp \
 ../src/streamer.cpp

CONFIG += link_pkgconfig
PKGCONFIG += gstreamer-1.0
PKGCONFIG += glib-2.0

OTHER_FILES = $$files(rpm/*)

# just for QtCreator and glibconfig.h
INCLUDEPATH += "/usr/lib/x86_64-linux-gnu/glib-2.0/include/"
