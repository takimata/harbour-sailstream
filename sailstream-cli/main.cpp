#include <cstdlib>
#include <stdio.h>
#include <csignal>

#include "streamer.hpp"

#include <glib.h>
#include <gst/gst.h>

#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <memory>

// If you are new to GLib, read https://developer.gnome.org/programming-guidelines/unstable/memory-management.html.en to
// learn about GLib's memory management peculiarities

// see https://github.com/GStreamer/gst-rtsp-server/blob/master/gst/rtsp-server/rtsp-sdp.c for get_roc_from_stats

std::shared_ptr<Streamer> global_stream;

void signalHandler_INT(int num) {
  if (num != SIGINT) {
    return;
  }
  if (global_stream != nullptr) {
    global_stream->stop();
  }
}

void start(bool useMKI, Streamer::MediaSelection m, const gchar *dstIpAddress) {
  GMainLoop *loop = g_main_loop_new(nullptr, FALSE);

  Streamer::FinishedCb onFinished = [&loop]() {
                                      g_main_loop_quit(loop);
                                    };

  global_stream = std::make_shared<Streamer>(useMKI, m, dstIpAddress, onFinished);
  global_stream->start();

  g_print("Running main loop...\n");
  // GST_DEBUG_BIN_TO_DOT_FILE(GST_BIN(pipeline->pipeline), GST_DEBUG_GRAPH_SHOW_ALL, "test.dot");

  g_main_loop_run(loop);
  global_stream = nullptr;
  g_main_loop_unref(loop);
}

int main(int argc, char *argv[]) {
  gchar ip[] = "localhost";

  gchar *dstIpAddress = ip;
  gboolean useMKI = TRUE;
  gboolean video = TRUE;
  gboolean audio = TRUE;

  GOptionContext *ctx;
  GError *err = nullptr;
  GOptionEntry entries[] = {
    {"dstIpAddress", 'i', 0, G_OPTION_ARG_STRING, &dstIpAddress,
     "Data will be sent to <IP> port " STR(UDPVIDEOPORT) " (video) and port " STR(UDPAUDIOPORT) " (audio)", "IP"},
    {"nomki", 'c', G_OPTION_FLAG_REVERSE, G_OPTION_ARG_NONE, &useMKI,
     "Compatibility mode: don't use Master Key Identifiers. Specify this if receiver runs Gstreamer < 1.16", nullptr},
    {"deadpan", 'd', G_OPTION_FLAG_REVERSE, G_OPTION_ARG_NONE, &video, "Don't stream video", nullptr},
    {"silent", 's', G_OPTION_FLAG_REVERSE, G_OPTION_ARG_NONE, &audio, "Don't stream audio", nullptr},
    {},
  };

  ctx = g_option_context_new("- Camera+Mic UDP Streamer");
  g_option_context_add_main_entries(ctx, entries, nullptr);
  g_option_context_add_group(ctx, gst_init_get_option_group());
  // parse and remove parsed data from args
  if (!g_option_context_parse(ctx, &argc, &argv, &err)) {
    g_print("Failed to initialize: %s\n", err->message);
    g_clear_error(&err);
    g_option_context_free(ctx);
    return 1;
  }
  g_option_context_free(ctx);
  // parsing of own params done

  Streamer::MediaSelection streams;

  if (!audio && !video) {
    fprintf(stderr, "Specified --deadpan (no video) and --silent (no audio) => streaming nothing.\n");
    return 1;
  } else {
    if (audio && video) {
      streams = Streamer::MediaSelection::audiovideo;
    } else if (audio) {
      streams = Streamer::MediaSelection::audio;
    } else {
      streams = Streamer::MediaSelection::video;
    }
  }

  gst_init(&argc, &argv);
  signal(SIGINT, signalHandler_INT);  // TODO use sigaction
  start(useMKI, streams, dstIpAddress);
  gst_deinit();
}
